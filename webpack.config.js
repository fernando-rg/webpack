const HtmlWebpackPlugin = require("html-webpack-plugin"),
    MiniCssExtractPlugin = require("mini-css-extract-plugin")


module.exports = {

    output: {
        filename: '[name].[contenthash].js',
    },
    module: {
        rules:[
            {
                test:/\.js$/i,
                exclude: /node_modules/,
                use:{
                    loader: "babel-loader"
                }
            },
            {
                test: /\.html$/i,
                exclude: /node_modules/,
                use: {
                    loader: "html-loader",
                    options: {
                        minimize:true
                    }
                }
            },
            {
                test: /\.css$/i,
                exclude: /node_modules/,
                use: [
                    MiniCssExtractPlugin.loader, "css-loader" 
                ]
            }
        ]
    },
    plugins: [
        new HtmlWebpackPlugin({
            template: "./src/index.html",
            filename: "./index.html"
        }),
        new MiniCssExtractPlugin()

    ]
}