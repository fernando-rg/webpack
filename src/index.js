import style from "./style.css"
import index2 from './index2.js';

  function setImgSrc (_element, _img) {
    _element.find('img').attr('src', _img)
    _element.find('img').attr('alt', _element.attr('alt'))
    // _element.find('img').attr('tabindex', 100)
    return _element
  }
  
  function getImagePath (_options, _default_options, _index, _image_type) {
  
    var _image_path = null
  
    if (
      (typeof _options !== 'undefined') &&
      (typeof _options[_index] !== 'undefined') &&
      (typeof _options[_index][_image_type] !== 'undefined')
    ) {
      _image_path = _options[_index][_image_type]
    } else {
      if (
        (typeof _default_options !== 'undefined') &&
        (typeof _default_options[_index] !== 'undefined') &&
        (typeof _default_options[_index][_image_type] !== 'undefined')
      ) {
        _image_path = _default_options[_index][_image_type]
      } else {
        _image_path = (new ResponseFormatImages()).STARS_DEFAULT[_image_type]
      }
    }
    return _image_path
  }
  
  function starOnByIndex (_question, _index, _stars) {
  
    for (var _star in _stars) {
      var _star_element = _question.getStar(parseInt(_star))
  
      if (_index === parseInt(_star)) {
        var _img_on = translateImage(_stars[_star]['img_on'])
        _star_element.attr({ style: 'content:url(' + _img_on + ')' })
        setImgSrc(_star_element, _img_on) //IE EDGE
      } else {
        var _img_off = translateImage(_stars[_star]['img_off'])
        _star_element.attr({ style: 'content:url(' + _img_off + ')' })
        setImgSrc(_star_element, _img_off) //IE EDGE
      }
    }
  }
  
  function starOnByIndexAccesibility (_question, _index, _stars, tabindex) {
    for (var _star in _stars) {
      var _star_element = _question.getStar(parseInt(_star))
      tabindex += 1
      _star_element.attr('tabindex', tabindex)
      _star_element.attr('value', parseInt(_star))
      _star_element.find('img').attr('tabindex', tabindex)
      tabindex += 1
      _star_element.append('<div role="tooltip" aria-live="assertive" class="seleccionado text-to-translate" style="display:none">Seleccionado</div>')
      tabindex += 1
    }
    return tabindex
  }
  
  function starOnByIndexAndRight (_question, _index, _stars) {
  
    for (var _star in _stars) {
      var _star_element = _question.getStar(parseInt(_star))
  
      if (_index <= parseInt(_star)) {
        var _img_on = translateImage(_stars[_star]['img_on'])
        _star_element.attr({ style: 'content:url(' + _img_on + ')' })
        setImgSrc(_star_element, _img_on) //IE EDGE
      } else {
        var _img_off = translateImage(_stars[_star]['img_off'])
        _star_element.attr({ style: 'content:url(' + _img_off + ')' })
        setImgSrc(_star_element, _img_off) //IE EDGE
      }
    }
  }
  
  function addSlider (_idQuestion, _min, _max, _valueBegin, _options) {
  
    var _label     = document.createElement('label')
    var _container = document.createElement('div')
    var nodeText   = document.createTextNode('Desliza la barra para dejar su valoración')
    var _div       = document.createElement('div')
    var _img       = document.createElement('img')
  
    _img.setAttribute('class', 'sliderImg')
    _img.setAttribute('id', 'sliderImg' + _idQuestion)
  
    _label.setAttribute('id', 'textoSlider')
    _label.setAttribute('for', 'amount')
    _label.append(nodeText)
  
    if (typeof (_valueBegin) === 'undefined') {
      _valueBegin = _max / 2
    }
    _img.setAttribute('src', (new ResponseFormatImages().NUMBERS_INITIALSTATE)) // _options[_valueBegin]['img_on']);
  
    _container.setAttribute('id', 'slider-' + _idQuestion)
  
    _div.setAttribute('id', 'slider-range-max' + _idQuestion)
    _div.setAttribute('class', 'slider-range-max')
  
    var _grupo_pregunta = $('.grupo-pregunta[data-id=\'' + _idQuestion + '\']')
    _grupo_pregunta.find('input[name=\'rating_rateandgrade\']').attr('value', _valueBegin)
  
    _container.append(_img)
    _container.append(_div)
    _container.append(_label)
    _grupo_pregunta.append(_container)
  
    if (typeof (_min) === 'undefined') {
      _min = 1
    }
    if (typeof (_max) === 'undefined') {
      _max = 10
    }
  
    $(function () {
      var _slider = $('#slider-range-max' + _idQuestion)
      _slider.slider({
                       range: 'max',
                       min:   _min,
                       max:   _max,
                       value: _valueBegin,
                       slide: function (event, _slider) {
                         $('#sliderImg' + _idQuestion).attr('src', _options[_slider.value]['img_on'])
  
                         //TODO move to Question.setResponseValue
                         var _question        = new Question(_idQuestion)
                         var _question_rating = _question.getElement().find('.rating-container')
                         var _new_width       = _slider.value * (10 / (_max - _min))
                         _filled_stars        = _question_rating.find('span.filled-stars')
                         _filled_stars.css('width', (_new_width) + '%')
                         _input = _question.getElement().find('input[name=\'rating_rateandgrade\']')
                         _input.val(_slider.value)
                         _question.getElement().trigger('question.value.change', _question)
  
                         var boton = _grupo_pregunta.closest('.panel').find('.pasar-pantalla').attr('id')
                         btnON('#' + boton)
  
                       }
                     })
      $('#amount').val(_slider.slider('value'))
    })
  }
  
  function ResponseFormatOptions (_question, _format, _options, _default_options) {
  
    var _this            = this
    this.question        = _question
    this.options         = _options
    this.default_options = _default_options
    this.stars           = null
  
    this.init = function () {
  
      this.stars = this.options
      _my_log('ResponseFormatOptions:' + Object.keys(this.stars).length)
  
      this.question.getElement().find('input[name=\'rating_rateandgrade\']').on('rating.change', function (event, value, caption) {
  
        var _input_value = _this.question.getElement().find('input[name=\'rating_rateandgrade\']').val()
  
        var _int_input_value = 0
        var _found           = false
  
        var _num_options = Object.keys(_this.stars).length
  
        for (var _star in _this.stars) {
          var _star_element = _question.getStar(parseInt(_star))
          _my_log(_star_element)
        }
  
        if (_input_value > 0) {
  
          if (_input_value <= 1) {
            _int_input_value = 1
          } else if (_input_value <= 2) {
            _int_input_value = 2
          } else if (_input_value <= 3) {
            _int_input_value = 3
          } else if (_input_value <= 4) {
            _int_input_value = 4
          } else if (_input_value <= 5) {
            _int_input_value = 5
          }
        }
      })
  
      this.question.getElement().find('input[name=\'rating_rateandgrade\']').on('rating.change', function (event, value, caption) {
  
        var _input_value = _this.question.getElement().find('input[name=\'rating_rateandgrade\']').val()
  
        var _int_input_value = 0
  
        if (_input_value > 0) {
  
          if (_input_value <= 1) {
            _int_input_value = 1
          } else if (_input_value <= 2) {
            _int_input_value = 2
          } else if (_input_value <= 3) {
            _int_input_value = 3
          } else if (_input_value <= 4) {
            _int_input_value = 4
          } else if (_input_value <= 5) {
            _int_input_value = 5
          }
  
          _this.question.getElement().find('input[name=\'rating_rateandgrade\']').val(_int_input_value)
          starOnByIndex(_this.question, _int_input_value, _this.stars)
  
        }
  
      })
  
      $(document).on('survey.ready', function () {
        _this.offImages()
      })
  
      //IE EDGE
      $('#main').ready(function () {
        starOnByIndex(_question, 0, _this.stars)
      })
  
      $('#main #logo-pie-pagina1').click(function () {
        starOnByIndex(_question, 0, _this.stars)
      })
  
    }
  
    this.offImages = function () {
      starOnByIndex(this.question, 0, this.stars)
    }
  
    this.clean = function () {
  
    }
  }
  
  function ResponseFormatFaces (_question, _format, _options) {
    if (Object.keys(_options).length === 6) {
      return new ResponseFormat6Options(_question, _format, _options, (new ResponseFormatImages()).FACES_EMOJI_5)
    }
    return new ResponseFormat5Options(_question, _format, _options, (new ResponseFormatImages()).FACES_EMOJI_5)
  }
  
  function ResponseFormatYesOrNo (_question, _format, _options) {
    return new ResponseFormat2Options(_question, _format, _options, (new ResponseFormatImages()).YES_NO_NPS_STYLE)
  }
  
  function ResponseFormat1Option (_question, _format, _options, _default_options) {
  
    var _this            = this
    this.question        = _question
    this.options         = _options
    this.default_options = _default_options
    this.stars           = null
  
    this.init = function () {
  
      this.stars = {
        '1': {
          'img_on':  getImagePath(this.options, this.default_options, 1, 'img_on'),
          'img_off': getImagePath(this.options, this.default_options, 1, 'img_off')
        },
      }
  
      this.question.getElement().find('input[name=\'rating_rateandgrade\']').on('rating.change', function (event, value, caption) {
        var _input_value = _this.question.getElement().find('input[name=\'rating_rateandgrade\']').val()
        if (_input_value > 0) {
          if (_input_value <= 5) {
            starOnByIndex(_this.question, 1, _this.stars)
          }
        }
      })
  
      $(document).on('survey.ready', function () {
        _this.offImages()
      })
  
      //IE EDGE
      $('#main').ready(function () {
        starOnByIndex(_question, 0, _this.stars)
      })
  
      $('#main #logo-pie-pagina1').click(function () {
        starOnByIndex(_question, 0, _this.stars)
      })
  
    }
  
    this.offImages = function () {
      starOnByIndex(this.question, 0, this.stars)
    }
  
  }
  
  function ResponseFormat2Options (_question, _format, _options, _default_options) {
  
    var _this            = this
    this.question        = _question
    this.options         = _options
    this.default_options = _default_options
    this.stars           = null
    this._change_count   = 0
  
    this.init         = function () {
  
      this.stars = {
        '1': {
          'img_on':  getImagePath(this.options, this.default_options, 1, 'img_on'),
          'img_off': getImagePath(this.options, this.default_options, 1, 'img_off')
        },
        '2': {
          'img_on':  getImagePath(this.options, this.default_options, 2, 'img_on'),
          'img_off': getImagePath(this.options, this.default_options, 2, 'img_off')
        },
      }
  
      this.question.getElement().find('input[name=\'rating_rateandgrade\']').on('rating.change', function (event, value, caption) {
  
        if (isTouchable()) {
          if (_this._change_count > 0) {
            _this._change_count = 0
            _this.eventHandler(event, value, caption)
          } else {
            _this._change_count = _this._change_count + 1
          }
        } else {
          _this.eventHandler(event, value, caption)
        }
  
      })
  
      $(document).on('survey.ready', function () {
        _this.offImages()
      })
  
      //IE EDGE
      $('#main').ready(function () {
        starOnByIndex(_question, 0, _this.stars)
      })
  
      $('#main #logo-pie-pagina1').click(function () {
        starOnByIndex(_question, 0, _this.stars)
      })
  
    }
    this.eventHandler = function (event, value, caption) {
  
      var _input_value     = _this.question.getElement().find('input[name=\'rating_rateandgrade\']').val()
      var _int_input_value = 0
      if (_input_value > 0) {
        if (_input_value <= 2.5) {
          _int_input_value = 1
        } else if (_input_value <= 5) {
          _int_input_value = 2
        }
        _this.question.getElement().find('input[name=\'rating_rateandgrade\']').val(_int_input_value)
        starOnByIndex(_this.question, _int_input_value, _this.stars)
      }
      _this.question.getElement().trigger('question.value.change', _this.question)
    }
  
    this.getRespoonseValue = function () {
      return _this.question.getElement().find('input[name=\'rating_rateandgrade\']').val()
    }
  
    this.setRespoonseValue = function (_value) {
      _this.question.getElement().find('input[name=\'rating_rateandgrade\']').rating('update', _value).val()
      starOnByIndex(_question, _value, _this.stars)
      _this.question.getElement().trigger('question.value.change', _this.question)
    }
  
    this.offImages = function () {
      starOnByIndex(this.question, 0, this.stars)
    }
  
  }
  
  function ResponseFormat3Options (_question, _format, _options, _default_options) {
  
    var _this            = this
    this.question        = _question
    this.options         = _options
    this.default_options = _default_options
    this.stars           = null
    this._change_count   = 0
  
    this.init         = function () {
  
      this.stars = {
        '1': {
          'img_on':  getImagePath(this.options, this.default_options, 1, 'img_on'),
          'img_off': getImagePath(this.options, this.default_options, 1, 'img_off')
        },
        '2': {
          'img_on':  getImagePath(this.options, this.default_options, 2, 'img_on'),
          'img_off': getImagePath(this.options, this.default_options, 2, 'img_off')
        },
        '3': {
          'img_on':  getImagePath(this.options, this.default_options, 3, 'img_on'),
          'img_off': getImagePath(this.options, this.default_options, 3, 'img_off')
        },
      }
  
      _this.question.getElement().find('input[name=\'rating_rateandgrade\']').on('rating.change', function (event, value, caption) {
  
        if (isTouchable()) {
          if (_this._change_count > 0) {
            _this._change_count = 0
            _this.eventHandler(event, value, caption)
          } else {
            _this._change_count = _this._change_count + 1
          }
        } else {
          _this.eventHandler(event, value, caption)
        }
  
      })
  
      $(document).on('survey.ready', function () {
        _this.offImages()
      })
  
      //IE EDGE
      $('#main').ready(function () {
        starOnByIndex(_question, 0, _this.stars)
      })
  
      $('#main #logo-pie-pagina1').click(function () {
        starOnByIndex(_question, 0, _this.stars)
      })
  
    }
    this.eventHandler = function (event, value, caption) {
  
      var _input_value     = _this.question.getElement().find('input[name=\'rating_rateandgrade\']').val()
      var _int_input_value = 0
      if (_input_value > 0) {
        if (_input_value <= 1.67) {
          _int_input_value = 1
        } else if (_input_value <= 3.33) {
          _int_input_value = 2
        } else if (_input_value <= 5) {
          _int_input_value = 3
        }
  
        _this.question.getElement().find('input[name=\'rating_rateandgrade\']').val(_int_input_value)
        starOnByIndex(_this.question, _int_input_value, _this.stars)
      }
      _this.question.getElement().trigger('question.value.change', _this.question)
    }
  
    this.getRespoonseValue = function () {
      return _this.question.getElement().find('input[name=\'rating_rateandgrade\']').val()
    }
  
    this.setRespoonseValue = function (_value) {
      _this.question.getElement().find('input[name=\'rating_rateandgrade\']').rating('update', _value).val()
      starOnByIndex(_question, _value, _this.stars)
      _this.question.getElement().trigger('question.value.change', _this.question)
    }
  
    this.offImages = function () {
      starOnByIndex(this.question, 0, this.stars)
    }
  }
  
  function ResponseFormat4Options (_question, _format, _options, _default_options) {
  
    var _this            = this
    this.question        = _question
    this.options         = _options
    this.default_options = _default_options
    this.stars           = null
    this._change_count   = 0
  
    this.init = function () {
  
      this.stars = {
        '1': {
          'img_on':  getImagePath(this.options, this.default_options, 1, 'img_on'),
          'img_off': getImagePath(this.options, this.default_options, 1, 'img_off')
        },
        '2': {
          'img_on':  getImagePath(this.options, this.default_options, 2, 'img_on'),
          'img_off': getImagePath(this.options, this.default_options, 2, 'img_off')
        },
        '3': {
          'img_on':  getImagePath(this.options, this.default_options, 3, 'img_on'),
          'img_off': getImagePath(this.options, this.default_options, 3, 'img_off')
        },
        '4': {
          'img_on':  getImagePath(this.options, this.default_options, 4, 'img_on'),
          'img_off': getImagePath(this.options, this.default_options, 4, 'img_off')
        }
      }
  
      _this.question.getElement().find('input[name=\'rating_rateandgrade\']').on('rating.change', function (event, value, caption) {
  
        if (isTouchable()) {
          if (_this._change_count > 0) {
            _this._change_count = 0
            _this.eventHandler(event, value, caption)
          } else {
            _this._change_count = _this._change_count + 1
          }
        } else {
          _this.eventHandler(event, value, caption)
        }
  
      })
  
      $(document).on('survey.ready', function () {
        _this.offImages()
      })
  
      //IE EDGE
      $('#main').ready(function () {
        starOnByIndex(_question, 0, _this.stars)
      })
  
      $('#main #logo-pie-pagina1').click(function () {
        starOnByIndex(_question, 0, _this.stars)
      })
  
      this.eventHandler = function (event, value, caption) {
  
        var _input_value     = _this.question.getElement().find('input[name=\'rating_rateandgrade\']').val()
        var _int_input_value = 0
        if (_input_value > 0) {
          if (_input_value <= 1.25) {
            _int_input_value = 1
          } else if (_input_value <= 2.50) {
            _int_input_value = 2
          } else if (_input_value <= 3.75) {
            _int_input_value = 3
          } else if (_input_value <= 5) {
            _int_input_value = 4
          }
          _this.question.getElement().find('input[name=\'rating_rateandgrade\']').val(_int_input_value)
          starOnByIndex(_this.question, _int_input_value, _this.stars)
        }
        _this.question.getElement().trigger('question.value.change', _this.question)
      }
  
      this.getRespoonseValue = function () {
        return _this.question.getElement().find('input[name=\'rating_rateandgrade\']').val()
      }
  
      this.setRespoonseValue = function (_value) {
        _this.question.getElement().find('input[name=\'rating_rateandgrade\']').rating('update', _value).val()
        starOnByIndex(_question, _value, _this.stars)
        _this.question.getElement().trigger('question.value.change', _this.question)
      }
  
      this.offImages = function () {
        starOnByIndex(this.question, 0, this.stars)
      }
    }
  }
  
  function ResponseFormat5Options (_question, _format, _options, _default_options) {
  
    var _this            = this
    this.question        = _question
    this.options         = _options
    this.default_options = _default_options
    this.stars           = null
    this._change_count   = 0
  
    this.init         = function () {
  
      this.stars = {
        '1': {
          'img_on':  getImagePath(this.options, this.default_options, 1, 'img_on'),
          'img_off': getImagePath(this.options, this.default_options, 1, 'img_off')
        },
        '2': {
          'img_on':  getImagePath(this.options, this.default_options, 2, 'img_on'),
          'img_off': getImagePath(this.options, this.default_options, 2, 'img_off')
        },
        '3': {
          'img_on':  getImagePath(this.options, this.default_options, 3, 'img_on'),
          'img_off': getImagePath(this.options, this.default_options, 3, 'img_off')
        },
        '4': {
          'img_on':  getImagePath(this.options, this.default_options, 4, 'img_on'),
          'img_off': getImagePath(this.options, this.default_options, 4, 'img_off')
        },
        '5': {
          'img_on':  getImagePath(this.options, this.default_options, 5, 'img_on'),
          'img_off': getImagePath(this.options, this.default_options, 5, 'img_off')
        }
      }
  
      _this.question.getElement().find('input[name=\'rating_rateandgrade\']').on('rating.change', function (event, value, caption) {
  
        if (isTouchable()) {
          if (_this._change_count > 0) {
            _this._change_count = 0
            _this.eventHandler(event, value, caption)
          } else {
            _this._change_count = _this._change_count + 1
          }
        } else {
          _this.eventHandler(event, value, caption)
        }
  
      })
  
      $(document).on('survey.ready', function () {
        _this.offImages()
        _this.question.getElement().find('span.star').on('keypress', function (event) {
          var _value = $(this).attr('value')
          var _input = _this.question.getElement().find('input[name=\'rating_rateandgrade\']')
          _input.rating('update', _value).val()
          _input.val($(this).attr('value'))
          _input.trigger('change').trigger('rating.change', [_input.val(), ''])
          _this.question.getElement().trigger('question.value.change', _this.question)
          $(this).closest('.rating').find('.seleccionado').css('display', 'none')
          $(this).find('.seleccionado').css('display', 'block')
          $(this).find('.seleccionado').focus()
        })
      })
  
      //IE EDGE
      $('#main').ready(function () {
        starOnByIndex(_question, 0, _this.stars)
      })
  
      $('#main #logo-pie-pagina1').click(function () {
        starOnByIndex(_question, 0, _this.stars)
      })
  
    }
    this.eventHandler = function (event, value, caption) {
  
      var _input_value     = _this.question.getElement().find('input[name=\'rating_rateandgrade\']').val()
      var _int_input_value = 0
      if (_input_value > 0) {
        if (_input_value <= 1) {
          _int_input_value = 1
        } else if (_input_value <= 2) {
          _int_input_value = 2
        } else if (_input_value <= 3) {
          _int_input_value = 3
        } else if (_input_value <= 4) {
          _int_input_value = 4
        } else if (_input_value <= 5) {
          _int_input_value = 5
        }
        _this.question.getElement().find('input[name=\'rating_rateandgrade\']').val(_int_input_value)
        starOnByIndex(_this.question, _int_input_value, _this.stars)
      }
      _this.question.getElement().trigger('question.value.change', _this.question)
    }
  
    this.getRespoonseValue = function () {
      return _this.question.getElement().find('input[name=\'rating_rateandgrade\']').val()
    }
  
    this.setRespoonseValue = function (_value) {
      _this.question.getElement().find('input[name=\'rating_rateandgrade\']').rating('update', _value).val()
      starOnByIndex(_question, _value, _this.stars)
      _this.question.getElement().trigger('question.value.change', _this.question)
    }
  
    this.offImages = function () {
      starOnByIndex(this.question, 0, this.stars)
    }
  
    this.accesibility = function (tabindex) {
      return starOnByIndexAccesibility(_question, 0, _this.stars, tabindex)
    }
  
  }
  
  function ResponseFormat5OptionsRTL (_question, _format, _options, _default_options) {
  
    _my_log('ResponseFormat5OptionsRTL')
    var _this            = this
    this.question        = _question
    this.options         = _options
    this.default_options = _default_options
    this.stars           = null
  
    this.init = function () {
  
      this.stars = {
        '1': {
          'img_on':  getImagePath(this.options, this.default_options, 1, 'img_on'),
          'img_off': getImagePath(this.options, this.default_options, 1, 'img_off')
        },
        '2': {
          'img_on':  getImagePath(this.options, this.default_options, 2, 'img_on'),
          'img_off': getImagePath(this.options, this.default_options, 2, 'img_off')
        },
        '3': {
          'img_on':  getImagePath(this.options, this.default_options, 3, 'img_on'),
          'img_off': getImagePath(this.options, this.default_options, 3, 'img_off')
        },
        '4': {
          'img_on':  getImagePath(this.options, this.default_options, 4, 'img_on'),
          'img_off': getImagePath(this.options, this.default_options, 4, 'img_off')
        },
        '5': {
          'img_on':  getImagePath(this.options, this.default_options, 5, 'img_on'),
          'img_off': getImagePath(this.options, this.default_options, 5, 'img_off')
        }
      }
  
      this.question.getElement().find('input[name=\'rating_rateandgrade\']').on('rating.change', function (event, value, caption) {
        var _input_value     = _this.question.getElement().find('input[name=\'rating_rateandgrade\']').val()
        var _int_input_value = 0
        if (_input_value > 0) {
          if (_input_value <= 1) {
            _int_input_value = 1
          } else if (_input_value <= 2) {
            _int_input_value = 2
          } else if (_input_value <= 3) {
            _int_input_value = 3
          } else if (_input_value <= 4) {
            _int_input_value = 4
          } else if (_input_value <= 5) {
            _int_input_value = 5
          }
          _this.question.getElement().find('input[name=\'rating_rateandgrade\']').val(_int_input_value)
          starOnByIndexAndRight(_this.question, _int_input_value, _this.stars)
        }
      })
  
      $(document).on('survey.ready', function () {
        _this.offImages()
      })
  
      //IE EDGE
      $('#main').ready(function () {
        starOnByIndexAndRight(_question, 0, _this.stars)
      })
  
      $('#main #logo-pie-pagina1').click(function () {
        starOnByIndexAndRight(_question, 0, _this.stars)
      })
  
    }
  
    this.getRespoonseValue = function () {
      return _this.question.getElement().find('input[name=\'rating_rateandgrade\']').val()
    }
  
    this.setRespoonseValue = function (_value) {
      _this.question.getElement().find('input[name=\'rating_rateandgrade\']').rating('update', _value).val()
      starOnByIndex(_question, _value, _this.stars)
      _this.question.getElement().trigger('question.value.change', _this.question)
    }
  
    this.offImages = function () {
      starOnByIndex(this.question, 0, this.stars)
    }
  
  }
  
  function ResponseFormat6Options (_question, _format, _options, _default_options) {
  
    _my_log('6 OPTIONS:')
  
    var _this            = this
    this.question        = _question
    this.options         = _options
    this.default_options = _default_options
    this.stars           = null
    this._change_count   = 0
  
    this.init = function () {
  
      this.stars = {
        '1': {
          'img_on':  getImagePath(this.options, this.default_options, 1, 'img_on'),
          'img_off': getImagePath(this.options, this.default_options, 1, 'img_off')
        },
        '2': {
          'img_on':  getImagePath(this.options, this.default_options, 2, 'img_on'),
          'img_off': getImagePath(this.options, this.default_options, 2, 'img_off')
        },
        '3': {
          'img_on':  getImagePath(this.options, this.default_options, 3, 'img_on'),
          'img_off': getImagePath(this.options, this.default_options, 3, 'img_off')
        },
        '4': {
          'img_on':  getImagePath(this.options, this.default_options, 4, 'img_on'),
          'img_off': getImagePath(this.options, this.default_options, 4, 'img_off')
        },
        '5': {
          'img_on':  getImagePath(this.options, this.default_options, 5, 'img_on'),
          'img_off': getImagePath(this.options, this.default_options, 5, 'img_off')
        },
        '6': {
          'img_on':  getImagePath(this.options, this.default_options, 6, 'img_on'),
          'img_off': getImagePath(this.options, this.default_options, 6, 'img_off')
        }
      }
  
      this.question.getElement().find('input[name=\'rating_rateandgrade\']').on('rating.change', function (event, value, caption) {
  
        _my_log('6 OPTIONS:2')
        var __isTouchable = 'ontouchstart' in window
        if (__isTouchable) {
          if (_this._change_count > 0) {
            _this._change_count = 0
            _this.eventHandler(event, value, caption)
          } else {
            _this._change_count = _this._change_count + 1
          }
        } else {
          _this.eventHandler(event, value, caption)
        }
  
      })
  
      $(document).on('survey.ready', function () {
        _this.offImages()
        _this.question.getElement().find('span.star').on('keypress', function (event) {
          var _value = $(this).attr('value')
          var _input = _this.question.getElement().find('input[name=\'rating_rateandgrade\']')
          _input.rating('update', _value).val()
          _input.val($(this).attr('value'))
          _input.trigger('change').trigger('rating.change', [_input.val(), ''])
          _this.question.getElement().trigger('question.value.change', _this.question)
          $(this).closest('.rating').find('.seleccionado').css('display', 'none')
          $(this).find('.seleccionado').css('display', 'block')
          $(this).find('.seleccionado').focus()
        })
      })
  
      //IE EDGE
      $('#main').ready(function () {
        starOnByIndex(_question, 0, _this.stars)
      })
  
      $('#main #logo-pie-pagina1').click(function () {
        starOnByIndex(_question, 0, _this.stars)
      })
  
    }
  
    this.eventHandler = function (event, value, caption) {
      _my_log('6 OPTIONS:3')
  
      var _input_value     = _this.question.getElement().find('input[name=\'rating_rateandgrade\']').val()
      var _int_input_value = 0
      if (_input_value > 0) {
        if (_input_value <= 1) {
          _int_input_value = 1
        } else if (_input_value <= 2) {
          _int_input_value = 2
        } else if (_input_value <= 3) {
          _int_input_value = 3
        } else if (_input_value <= 4) {
          _int_input_value = 4
        } else if (_input_value <= 5) {
          _int_input_value = 5
        } else if (_input_value <= 6) {
          _int_input_value = 6
        }
        _my_log('SURVEY 6 OPTIONS:' + _int_input_value)
        _this.question.getElement().find('input[name=\'rating_rateandgrade\']').val(_int_input_value)
        starOnByIndex(_this.question, _int_input_value, _this.stars)
      }
      _this.question.getElement().trigger('question.value.change', _this.question)
    }
  
    this.getRespoonseValue = function () {
      return _this.question.getElement().find('input[name=\'rating_rateandgrade\']').val()
    }
  
    this.setRespoonseValue = function (_value) {
      _this.question.getElement().find('input[name=\'rating_rateandgrade\']').rating('update', _value).val()
      starOnByIndex(_question, _value, _this.stars)
      _this.question.getElement().trigger('question.value.change', _this.question)
    }
  
    this.offImages = function () {
      starOnByIndex(this.question, 0, this.stars)
    }
  
    this.accesibility = function (tabindex) {
      return starOnByIndexAccesibility(_question, 0, _this.stars, tabindex)
    }
  }
  
  function ResponseFormat9Options (_question, _format, _options, _default_options) {
  
    //_my_log('| ResponseFormat10Options function .question:'+_question +'format:'+_format);
  
    var _this            = this
    this.question        = _question
    this.options         = _options
    this.default_options = _default_options
    this.stars           = null
  
    this.init = function () {
      //_my_log('| ResponseFormat10Options.init:'+_question +'format:'+_format);
  
      this.stars = {
        '1': {
          'img_on':  getImagePath(this.options, this.default_options, 1, 'img_on'),
          'img_off': getImagePath(this.options, this.default_options, 1, 'img_off')
        },
        '2': {
          'img_on':  getImagePath(this.options, this.default_options, 2, 'img_on'),
          'img_off': getImagePath(this.options, this.default_options, 2, 'img_off')
        },
        '3': {
          'img_on':  getImagePath(this.options, this.default_options, 3, 'img_on'),
          'img_off': getImagePath(this.options, this.default_options, 3, 'img_off')
        },
        '4': {
          'img_on':  getImagePath(this.options, this.default_options, 4, 'img_on'),
          'img_off': getImagePath(this.options, this.default_options, 4, 'img_off')
        },
        '5': {
          'img_on':  getImagePath(this.options, this.default_options, 5, 'img_on'),
          'img_off': getImagePath(this.options, this.default_options, 5, 'img_off')
        },
        '6': {
          'img_on':  getImagePath(this.options, this.default_options, 6, 'img_on'),
          'img_off': getImagePath(this.options, this.default_options, 6, 'img_off')
        },
        '7': {
          'img_on':  getImagePath(this.options, this.default_options, 7, 'img_on'),
          'img_off': getImagePath(this.options, this.default_options, 7, 'img_off')
        },
        '8': {
          'img_on':  getImagePath(this.options, this.default_options, 8, 'img_on'),
          'img_off': getImagePath(this.options, this.default_options, 8, 'img_off')
        },
        '9': {
          'img_on':  getImagePath(this.options, this.default_options, 9, 'img_on'),
          'img_off': getImagePath(this.options, this.default_options, 9, 'img_off')
        },
      }
  
      this.question.getElement().find('input[name=\'rating_rateandgrade\']').on('rating.change', function (event, value, caption) {
        var _input_value     = _this.question.getElement().find('input[name=\'rating_rateandgrade\']').val()
        var _int_input_value = 0
        if (_input_value > 0) {
          if (_input_value <= 1) {
            _int_input_value = 1
          } else if (_input_value <= 2) {
            _int_input_value = 2
          } else if (_input_value <= 3) {
            _int_input_value = 3
          } else if (_input_value <= 4) {
            _int_input_value = 4
          } else if (_input_value <= 5) {
            _int_input_value = 5
          } else if (_input_value <= 6) {
            _int_input_value = 6
          } else if (_input_value <= 7) {
            _int_input_value = 7
          } else if (_input_value <= 8) {
            _int_input_value = 8
          } else if (_input_value <= 9) {
            _int_input_value = 9
          }
          _this.question.getElement().find('input[name=\'rating_rateandgrade\']').val(_int_input_value)
          starOnByIndex(_this.question, _int_input_value, _this.stars)
        }
      })
  
      $(document).on('survey.ready', function () {
        _this.offImages()
      })
  
      //IE EDGE
      $('#main').ready(function () {
        starOnByIndex(_question, 0, _this.stars)
      })
  
      $('#main #logo-pie-pagina1').click(function () {
        starOnByIndex(_question, 0, _this.stars)
      })
  
    }
  
    this.getRespoonseValue = function () {
      return _this.question.getElement().find('input[name=\'rating_rateandgrade\']').val()
    }
  
    this.setRespoonseValue = function (_value) {
      _this.question.getElement().find('input[name=\'rating_rateandgrade\']').rating('update', _value).val()
      starOnByIndex(_question, _value, _this.stars)
      _this.question.getElement().trigger('question.value.change', _this.question)
    }
  
    this.offImages = function () {
      starOnByIndex(this.question, 0, this.stars)
    }
  
  }
  
  function ResponseFormat10Options (_question, _format, _options, _default_options, _formatOptions) {
  
    //_my_log('| ResponseFormat11Options function .question:'+_question +'format:'+_format);
    var _this            = this
    this.question        = _question
    this.options         = _options
    this.default_options = _default_options
    this.stars           = null
    this.slider          = false
    this._change_count   = 0
  
    if (typeof _formatOptions !== 'undefined') {
      _this.slider = _formatOptions
    }
  
    this.enableSlider = function () {
  
      if ($(window).width() <= 767) {
        $('.rating-laboratorio[data-id=' + _question['id'] + ']').css('display', 'none')
        addSlider(_question['id'], 1, 10, 6, _options)
      } else {
        $('.rating-laboratorio[data-id=' + _question['id'] + ']').css('display', 'unset')
        $('div.slider-range-max').css('display', 'none')
        $('p.pSlider').css('display', 'none')
        starOnByIndex(_question, 0, _this.stars)
      }
    }
  
    this.init = function () {
      this._change_count = 0
  
      this.stars = {
        '1':  {
          'img_on':  getImagePath(this.options, this.default_options, 1, 'img_on'),
          'img_off': getImagePath(this.options, this.default_options, 1, 'img_off')
        },
        '2':  {
          'img_on':  getImagePath(this.options, this.default_options, 2, 'img_on'),
          'img_off': getImagePath(this.options, this.default_options, 2, 'img_off')
        },
        '3':  {
          'img_on':  getImagePath(this.options, this.default_options, 3, 'img_on'),
          'img_off': getImagePath(this.options, this.default_options, 3, 'img_off')
        },
        '4':  {
          'img_on':  getImagePath(this.options, this.default_options, 4, 'img_on'),
          'img_off': getImagePath(this.options, this.default_options, 4, 'img_off')
        },
        '5':  {
          'img_on':  getImagePath(this.options, this.default_options, 5, 'img_on'),
          'img_off': getImagePath(this.options, this.default_options, 5, 'img_off')
        },
        '6':  {
          'img_on':  getImagePath(this.options, this.default_options, 6, 'img_on'),
          'img_off': getImagePath(this.options, this.default_options, 6, 'img_off')
        },
        '7':  {
          'img_on':  getImagePath(this.options, this.default_options, 7, 'img_on'),
          'img_off': getImagePath(this.options, this.default_options, 7, 'img_off')
        },
        '8':  {
          'img_on':  getImagePath(this.options, this.default_options, 8, 'img_on'),
          'img_off': getImagePath(this.options, this.default_options, 8, 'img_off')
        },
        '9':  {
          'img_on':  getImagePath(this.options, this.default_options, 9, 'img_on'),
          'img_off': getImagePath(this.options, this.default_options, 9, 'img_off')
        },
        '10': {
          'img_on':  getImagePath(this.options, this.default_options, 10, 'img_on'),
          'img_off': getImagePath(this.options, this.default_options, 10, 'img_off')
        },
      }
  
      this.question.getElement().find('input[name=\'rating_rateandgrade\']').on('rating.change', function (event, value, caption) {
  
        var __isTouchable = 'ontouchstart' in window
        if (__isTouchable) {
          if (_this._change_count > 0) {
            _this._change_count = 0
            _this.eventHandler(event, value, caption)
          } else {
            _this._change_count = _this._change_count + 1
          }
        } else {
          _this.eventHandler(event, value, caption)
        }
  
      })
  
      $(document).on('survey.ready', function () {
        _this.offImages()
      })
  
      //IE EDGE
      $('#main').ready(function () {
        if (_this.slider) {
          _this.enableSlider()
        } else {
          starOnByIndex(_question, 0, _this.stars)
        }
      })
  
      $('#main #logo-pie-pagina1').click(function () {
        if (_this.slider) {
          _this.enableSlider()
        } else {
          starOnByIndex(_question, 0, _this.stars)
        }
      })
  
    }
  
    this.eventHandler = function (event, value, caption) {
      var _input_value     = _this.question.getElement().find('input[name=\'rating_rateandgrade\']').val()
      var _int_input_value = 0
      if (_input_value > 0) {
        if (_input_value <= 1) {
          _int_input_value = 1
        } else if (_input_value <= 2) {
          _int_input_value = 2
        } else if (_input_value <= 3) {
          _int_input_value = 3
        } else if (_input_value <= 4) {
          _int_input_value = 4
        } else if (_input_value <= 5) {
          _int_input_value = 5
        } else if (_input_value <= 6) {
          _int_input_value = 6
        } else if (_input_value <= 7) {
          _int_input_value = 7
        } else if (_input_value <= 8) {
          _int_input_value = 8
        } else if (_input_value <= 9) {
          _int_input_value = 9
        } else if (_input_value <= 10) {
          _int_input_value = 10
        }
        _this.question.getElement().find('input[name=\'rating_rateandgrade\']').val(_int_input_value)
        starOnByIndex(_this.question, _int_input_value, _this.stars)
        _this.question.getElement().trigger('question.value.change', _this.question)
      }
    }
  
    this.getRespoonseValue = function () {
      return _this.question.getElement().find('input[name=\'rating_rateandgrade\']').val()
    }
  
    this.setRespoonseValue = function (_value) {
      _this.question.getElement().find('input[name=\'rating_rateandgrade\']').rating('update', _value).val()
      starOnByIndex(_question, _value, _this.stars)
      if ($(window).width() <= 767) {
  
        $('div#slider-' + _this.question.getId()).each(function (index, element) {
          $(element).remove()
          addSlider(_this.question.getId(), 1, 10, _value, _options)
        })
  
      }
      _this.question.getElement().trigger('question.value.change', _this.question)
    }
  
    this.offImages = function () {
      starOnByIndex(this.question, 0, this.stars)
    }
  
    $(window).resize(function () {
      if ($(window).width() <= 767 && _this.slider) {
        $('div.slider-range-max').css('display', 'block')
        $('p.pSlider').css('display', 'block')
        $('.rating-laboratorio[data-id=' + _question['id'] + ']').css('display', 'none')
        if ($('.sliderImg').length < 1) {
          _this.enableSlider()
        }
      }
      return
    })
  }
  
  function ResponseFormat11Options (_question, _format, _options, _default_options, _formatOptions) {
  
    //_my_log('| ResponseFormat11Options function .question:'+_question +'format:'+_format);
    var _this            = this
    this.question        = _question
    this.options         = _options
    this.default_options = _default_options
    this.stars           = null
    this.slider          = false
    this._change_count   = 0
  
    if (typeof _formatOptions !== 'undefined') {
      _this.slider = _formatOptions
    }
  
    this.enableSlider = function () {
  
      if ($(window).width() <= 767) {
        $('.rating-laboratorio[data-id=' + _question['id'] + ']').css('display', 'none')
        addSlider(_question['id'], 1, 11, 6, _options)
      } else {
        $('.rating-laboratorio[data-id=' + _question['id'] + ']').css('display', 'unset')
        $('div.slider-range-max').css('display', 'none')
        $('p.pSlider').css('display', 'none')
        starOnByIndex(_question, 0, _this.stars)
      }
    }
  
    this.init = function () {
      this._change_count = 0
  
      this.stars = {
        '1':  {
          'img_on':  getImagePath(this.options, this.default_options, 1, 'img_on'),
          'img_off': getImagePath(this.options, this.default_options, 1, 'img_off')
        },
        '2':  {
          'img_on':  getImagePath(this.options, this.default_options, 2, 'img_on'),
          'img_off': getImagePath(this.options, this.default_options, 2, 'img_off')
        },
        '3':  {
          'img_on':  getImagePath(this.options, this.default_options, 3, 'img_on'),
          'img_off': getImagePath(this.options, this.default_options, 3, 'img_off')
        },
        '4':  {
          'img_on':  getImagePath(this.options, this.default_options, 4, 'img_on'),
          'img_off': getImagePath(this.options, this.default_options, 4, 'img_off')
        },
        '5':  {
          'img_on':  getImagePath(this.options, this.default_options, 5, 'img_on'),
          'img_off': getImagePath(this.options, this.default_options, 5, 'img_off')
        },
        '6':  {
          'img_on':  getImagePath(this.options, this.default_options, 6, 'img_on'),
          'img_off': getImagePath(this.options, this.default_options, 6, 'img_off')
        },
        '7':  {
          'img_on':  getImagePath(this.options, this.default_options, 7, 'img_on'),
          'img_off': getImagePath(this.options, this.default_options, 7, 'img_off')
        },
        '8':  {
          'img_on':  getImagePath(this.options, this.default_options, 8, 'img_on'),
          'img_off': getImagePath(this.options, this.default_options, 8, 'img_off')
        },
        '9':  {
          'img_on':  getImagePath(this.options, this.default_options, 9, 'img_on'),
          'img_off': getImagePath(this.options, this.default_options, 9, 'img_off')
        },
        '10': {
          'img_on':  getImagePath(this.options, this.default_options, 10, 'img_on'),
          'img_off': getImagePath(this.options, this.default_options, 10, 'img_off')
        },
        '11': {
          'img_on':  getImagePath(this.options, this.default_options, 11, 'img_on'),
          'img_off': getImagePath(this.options, this.default_options, 11, 'img_off')
        }
      }
  
      this.question.getElement().find('input[name=\'rating_rateandgrade\']').on('rating.change', function (event, value, caption) {
  
        var __isTouchable = 'ontouchstart' in window
        if (__isTouchable) {
          if (_this._change_count > 0) {
            _this._change_count = 0
            _this.eventHandler(event, value, caption)
          } else {
            _this._change_count = _this._change_count + 1
          }
        } else {
          _this.eventHandler(event, value, caption)
        }
  
      })
  
      $(document).on('survey.ready', function () {
        _this.offImages()
      })
  
      //IE EDGE
      $('#main').ready(function () {
        if (_this.slider) {
          _this.enableSlider()
        } else {
          starOnByIndex(_question, 0, _this.stars)
        }
      })
  
      $('#main #logo-pie-pagina1').click(function () {
        if (_this.slider) {
          _this.enableSlider()
        } else {
          starOnByIndex(_question, 0, _this.stars)
        }
      })
  
    }
  
    this.eventHandler = function (event, value, caption) {
      var _input_value     = _this.question.getElement().find('input[name=\'rating_rateandgrade\']').val()
      var _int_input_value = 0
      if (_input_value > 0) {
        if (_input_value <= 1) {
          _int_input_value = 1
        } else if (_input_value <= 2) {
          _int_input_value = 2
        } else if (_input_value <= 3) {
          _int_input_value = 3
        } else if (_input_value <= 4) {
          _int_input_value = 4
        } else if (_input_value <= 5) {
          _int_input_value = 5
        } else if (_input_value <= 6) {
          _int_input_value = 6
        } else if (_input_value <= 7) {
          _int_input_value = 7
        } else if (_input_value <= 8) {
          _int_input_value = 8
        } else if (_input_value <= 9) {
          _int_input_value = 9
        } else if (_input_value <= 10) {
          _int_input_value = 10
        } else if (_input_value <= 11) {
          _int_input_value = 11
        }
        _this.question.getElement().find('input[name=\'rating_rateandgrade\']').val(_int_input_value)
        starOnByIndex(_this.question, _int_input_value, _this.stars)
        _this.question.getElement().trigger('question.value.change', _this.question)
      }
    }
  
    this.getRespoonseValue = function () {
      return _this.question.getElement().find('input[name=\'rating_rateandgrade\']').val()
    }
  
    this.setRespoonseValue = function (_value) {
      _this.question.getElement().find('input[name=\'rating_rateandgrade\']').rating('update', _value).val()
      starOnByIndex(_question, _value, _this.stars)
      _this.question.getElement().trigger('question.value.change', _this.question)
    }
  
    this.offImages = function () {
      starOnByIndex(this.question, 0, this.stars)
    }
  
    $(window).resize(function () {
      if ($(window).width() <= 767 && _this.slider) {
        $('div.slider-range-max').css('display', 'block')
        $('p.pSlider').css('display', 'block')
        $('.rating-laboratorio[data-id=' + _question['id'] + ']').css('display', 'none')
        if ($('.sliderImg').length < 1) {
          _this.enableSlider()
        }
      }
      return
    })
  
  }
  
  function ResponseFormatRadioGroup (_question, _format, _options) {
  
    var _this        = this
    this.question    = _question
    this.options     = _options
    this._radiogroup = new RgRadioGroup(_question.id, _this.options)
  
    this.init = function () {
      // $('#main').ready(function () {
        _this.render()
        _question.getElement().find('input[type="radio"]').change(function (event) {
          _input = _this.question.getElement().find('input[name=\'rating_rateandgrade\']')
          _input.val($(this).attr('value'))
          _input.trigger('change').trigger('rating.change', [_input.val(), ''])
          console.log('Trigger: question.value.change')
          _this.question.getElement().trigger('question.value.change', _this.question)
        })
      // })
    }
  
    this.render = function () {
      _this.removeStarRating(_question)
      var _question_element           = _question.getElement()
      var _rating_laboratorio_element = _question_element.find('.rating-laboratorio')
      var _rating_container_element   = _rating_laboratorio_element.find('.rating-container')
      _rating_container_element.removeClass('rating-md')
      _rating_container_element.removeClass('rating-animate')
      var _radiogroup = new RgRadioGroup(_question.id, _this.options)
      _this._radiogroup.render(_rating_container_element)
    }
  
    this.removeStarRating = function (_question) {
      var _div_rating_laboratorio = _question.getElement().find('div.rating')
      _div_rating_laboratorio.remove()
    }
  
    this.getRespoonseValue = function () {
      return _this.question.getElement().find('input[name=\'rating_rateandgrade\']').val()
    }
  
    this.enable = function () {
      this._radiogroup.enable()
    }
  
    this.disable = function () {
      this._radiogroup.disable()
    }
  
    this.setRespoonseValue = function (_value) {
      _this._radiogroup.setRespoonseValue(_value)
      _this.question.getElement().find('input[name=\'rating_rateandgrade\']').val(_value)
      _this.question.getElement().find('input[name=\'rating_rateandgrade\']').trigger('change', _value) //TODO
      _this.question.getElement().trigger('question.value.change', _this.question)
    }
  
    this.accesibility = function (tabindex) {
      this.question.getElement().find('.radiogroup-element input[type="radio"]').each(function () {
        $(this).attr('tabindex', tabindex)
        tabindex += 1
      })
      return tabindex
    }
  
  }
  
  function ResponseFormatRadioGroupthisandthat (_question, _format, _options) {
  
    var _this        = this
    this.question    = _question
    this.options     = _options
    this._radiogroup = new RgRadioGroupthisandthat(_question.id, _this.options)
  
    this.init = function () {
      // $('#main').ready(function () {
        _this.render()
        _question.getElement().find('label').on(isTouchstartOrClick(), function (event) {
          var _input_radio_value = $(this).parent().find('input[type="radio"]').attr('value')
          _input                 = _this.question.getElement().find('input[name=\'rating_rateandgrade\']')
          _input.val(_input_radio_value)
          _input.trigger('change').trigger('rating.change', [_input.val(), ''])
          _this.question.getElement().trigger('question.value.change', _this.question)
        })
      // })
  
    }
  
    this.render = function () {
      _this.removeStarRating(_question)
      var _question_element           = _question.getElement()
      var _rating_laboratorio_element = _question_element.find('.rating-laboratorio')
      var _rating_container_element   = _rating_laboratorio_element.find('.rating-container')
      _rating_container_element.removeClass('rating-md')
      _rating_container_element.removeClass('rating-animate')
      var _radiogroup = new RgRadioGroupthisandthat(_question.id, _this.options)
      _this._radiogroup.render(_rating_container_element)
    }
  
    this.removeStarRating = function (_question) {
      var _div_rating_laboratorio = _question.getElement().find('div.rating')
      _div_rating_laboratorio.remove()
    }
  
    this.getRespoonseValue = function () {
      return _this.question.getElement().find('input[name=\'rating_rateandgrade\']').val()
    }
  
    this.setRespoonseValue = function (_value) {
      _this._radiogroup.setRespoonseValue(_value)
      _this.question.getElement().find('input[name=\'rating_rateandgrade\']').val(_value)
      _this.question.getElement().find('input[name=\'rating_rateandgrade\']').trigger('change', _value) //TODO
      _this.question.getElement().trigger('question.value.change', _this.question)
    }
  
    this.accesibility = function (tabindex) {
      this.question.getElement().find('.radiogroup-element input[type="radio"]').each(function () {
        $(this).attr('tabindex', tabindex)
        tabindex += 1
      })
      return tabindex
    }
  
  }
  
  function ResponseFormatCheckboxGroup (_question, _format, _options, _format_options) {
  
    var _this            = this
    this.question        = _question
    this.options         = _options
    this._format_options = _format_options
    this._value          = []
    this._checboxgroup   = new RgCheckboxGroup(_question.id, _this.options)
  
    this.init = function () {
      // $('#main').ready(function () {
        _this.render()
        var _input = _this.question.getElement().find('input[name=\'rating_rateandgrade\']')
        _input.attr('name', 'rating_rateandgrade[]')
        _question.getElement().find('input[type="checkbox"]').change(function (event) {
  
          _input = _this.question.getElement().find('input[name=\'rating_rateandgrade[]\']')
  
          if (($(this).prop('checked') === true)) {
            if ((!_this._format_options || !_this._format_options['maxSelected']) || (_this._format_options && _this._format_options['maxSelected'] && _this._value.length < _this._format_options['maxSelected'])) {
              _this._value.push($(this).attr('value'))
            } else {
              $(this).prop('checked', false)
            }
          } else {
            _this._value = removeItemFromArr(_this._value, $(this).attr('value'))
          }
          if(_this._value.length == 0){
            _input.val(null)
          }else{
          _input.val(JSON.stringify(_this._value))
          }
          _this._checboxgroup.setRespoonseValue(_this._value)
          _input.trigger('change').trigger('rating.change', [_input.val(), ''])
          _this.question.getElement().trigger('question.value.change', _this.question)
        })
      // })
  
    }
  
    this.render = function () {
      _this.removeStarRating(_question)
      var _question_element           = _question.getElement()
      var _rating_laboratorio_element = _question_element.find('.rating-laboratorio')
      var _rating_container_element   = _rating_laboratorio_element.find('.rating-container')
      _rating_container_element.removeClass('rating-md')
      _rating_container_element.removeClass('rating-animate')
      var _checboxgroup = new RgCheckboxGroup(_question.id, _this.options)
      _this._checboxgroup.render(_rating_container_element)
    }
  
    this.removeStarRating = function (_question) {
      var _div_rating_laboratorio = _question.getElement().find('div.rating')
      _div_rating_laboratorio.remove()
    }
  
    this.getRespoonseValue = function () {
      return _this.question.getElement().find('input[name=\'rating_rateandgrade[]\']').val()
    }
  
    this.setRespoonseValue = function (_value) {
      // var _actual_value = _this.question.getElement().find('input[name=\'rating_rateandgrade[]\']').val()
      // _this._value.push(_value)
      // _this.question.getElement().find('input[name=\'rating_rateandgrade[]\']').val(_this._value)
      _this._value = _value
      _this._checboxgroup.setRespoonseValue(_value)
      _this.question.getElement().find('input[name=\'rating_rateandgrade[]\']').val(JSON.stringify(_value))
      _this.question.getElement().find('input[name=\'rating_rateandgrade[]\']').trigger('change', JSON.stringify(_value)) //TODO
      _this.question.getElement().trigger('question.value.change', _this.question)
    }
  
    this.accesibility = function (tabindex) {
      this.question.getElement().find('.checkboxgroup-element input[type="checkbox"]').each(function () {
        $(this).attr('tabindex', tabindex)
        tabindex += 1
      })
      return tabindex
    }
  
  }
  
  function ResponseFormatCheckboxSimple (_question, _format, _options, _format_options) {
  
    var _this            = this
    this.question        = _question
    this.options         = _options
    this._format_options = _format_options
    this._value          = []
    this._checkboxsimple   = new RgCheckboxSimple(_question.id, _this.options)
  
    this.init = function () {
      // $('#main').ready(function () {
        _this.render()
        var _input = _this.question.getElement().find('input[name=\'rating_rateandgrade\']')
        _input.attr('name', 'rating_rateandgrade[]')
        _question.getElement().find('input[type="checkbox"]').change(function (event) {
  
          _input = _this.question.getElement().find('input[name=\'rating_rateandgrade[]\']')
  
          if (($(this).prop('checked') === true)) {
            if ((!_this._format_options || !_this._format_options['maxSelected']) || (_this._format_options && _this._format_options['maxSelected'] && _this._value.length < _this._format_options['maxSelected'])) {
              _this._value.push($(this).attr('value'))
            } else {
              $(this).prop('checked', false)
            }
          } else {
            _this._value = removeItemFromArr(_this._value, $(this).attr('value'))
          }
          if(_this._value.length == 0){
            _input.val(null)
          }else{
          _input.val(JSON.stringify(_this._value))
          }
          _this._checkboxsimple.setRespoonseValue(_this._value)
          _input.trigger('change').trigger('rating.change', [_input.val(), ''])
          _this.question.getElement().trigger('question.value.change', _this.question)
        })
      // })
  
    }
  
    this.render = function () {
      _this.removeStarRating(_question)
      var _question_element           = _question.getElement()
      var _rating_laboratorio_element = _question_element.find('.rating-laboratorio')
      var _rating_container_element   = _rating_laboratorio_element.find('.rating-container')
      _rating_container_element.removeClass('rating-md')
      _rating_container_element.removeClass('rating-animate')
      var _checkboxsimple = new RgCheckboxGroup(_question.id, _this.options)
      _this._checkboxsimple.render(_rating_container_element)
    }
  
    this.removeStarRating = function (_question) {
      var _div_rating_laboratorio = _question.getElement().find('div.rating')
      _div_rating_laboratorio.remove()
    }
  
    this.getRespoonseValue = function () {
      return _this.question.getElement().find('input[name=\'rating_rateandgrade[]\']').val()
    }
  
    this.setRespoonseValue = function (_value) {
      _this._value = _value
      _this._checkboxsimple.setRespoonseValue(_value)
      _this.question.getElement().find('input[name=\'rating_rateandgrade[]\']').val(JSON.stringify(_value))
      _this.question.getElement().find('input[name=\'rating_rateandgrade[]\']').trigger('change', JSON.stringify(_value)) //TODO
      _this.question.getElement().trigger('question.value.change', _this.question)
    }
  
    this.accesibility = function (tabindex) {
      this.question.getElement().find('.checkboxgroup-element input[type="checkbox"]').each(function () {
        $(this).attr('tabindex', tabindex)
        tabindex += 1
      })
      return tabindex
    }
  
  }
  
  function ResponseFormatCheckboxGroupthisandthat (_question, _format, _options, _format_options) {
  
    var _this            = this
    this.question        = _question
    this.options         = _options
    this._format_options = _format_options
    this._value          = []
    this._checboxgroup   = new RgCheckboxGroupthisanthat(_question.id, _this.options)
  
    this.init = function () {
      // $('#main').ready(function () {
        _this.render()
        var _input = _this.question.getElement().find('input[name=\'rating_rateandgrade\']')
        _input.attr('name', 'rating_rateandgrade[]')
        _question.getElement().find('input[type="checkbox"]').change(function (event) {
  
          _input = _this.question.getElement().find('input[name=\'rating_rateandgrade[]\']')
  
          if (($(this).prop('checked') === true)) {
            if ((!_this._format_options || !_this._format_options['maxSelected']) || (_this._format_options && _this._format_options['maxSelected'] && _this._value.length < _this._format_options['maxSelected'])) {
              _this._value.push($(this).attr('value'))
            } else {
              $(this).prop('checked', false)
            }
  
          } else {
            _this._value = removeItemFromArr(_this._value, $(this).attr('value'))
          }
  
          _input.val(JSON.stringify(_this._value))
          _this._checboxgroup.setRespoonseValue(_this._value)
          _input.trigger('change').trigger('rating.change', [_input.val(), ''])
          _this.question.getElement().trigger('question.value.change', _this.question)
        })
      // })
    }
  
    this.render = function () {
      _this.removeStarRating(_question)
      var _question_element           = _question.getElement()
      var _rating_laboratorio_element = _question_element.find('.rating-laboratorio')
      var _rating_container_element   = _rating_laboratorio_element.find('.rating-container')
      _rating_container_element.removeClass('rating-md')
      _rating_container_element.removeClass('rating-animate')
      var _checboxgroup = new RgCheckboxGroupthisanthat(_question.id, _this.options)
      _this._checboxgroup.render(_rating_container_element)
    }
  
    this.removeStarRating = function (_question) {
      var _div_rating_laboratorio = _question.getElement().find('div.rating')
      _div_rating_laboratorio.remove()
    }
  
    this.getRespoonseValue = function () {
      return _this.question.getElement().find('input[name=\'rating_rateandgrade[]\']').val()
    }
  
    this.setRespoonseValue = function (_value) {
      var _actual_value = _this.question.getElement().find('input[name=\'rating_rateandgrade[]\']').val()
      _this._value.push(_value)
      _this.question.getElement().find('input[name=\'rating_rateandgrade[]\']').val(_this._value)
      _this._checboxgroup.setRespoonseValue(_this._value)
      _this.question.getElement().trigger('question.value.change', _this.question)
    }
  }
  
  function ResponseFormatSelect (_question, _format, _options, _format_options) {
  
    var _this     = this
    this.question = _question
    this.options  = _options
    this._select  = new RgSelect(_question.id, _this.options, _format_options)
  
    this.init = function () {
      // $().ready(function () {
        //TODO Cambiar, solución para borrar la select por si vuelvo a recargar opciones
        _question.getElement().find('fieldset').remove()
        _this.render()
  
        if (_this._select._multiple) {
          var _input = _this.question.getElement().find('input[name=\'rating_rateandgrade\']')
          _input.attr('name', 'rating_rateandgrade[]')
        }
  
        _question.getElement().find('select').change(function () {
          var selectVal = $(this).val()
  
          var input = null
          if (_this._select._multiple) {
            input = $(this).closest('.rating-container').find('input[name=\'rating_rateandgrade[]\']')
            input.val(JSON.stringify(selectVal))
          } else {
            input = $(this).closest('.rating-container').find('input[name=\'rating_rateandgrade\']')
            input.val(selectVal)
          }
  
          input.trigger('change').trigger('rating.change', [input.val(), ''])
          _this.question.getElement().trigger('question.value.change', _this.question)
        })
  
        _this.question.getElement().trigger('question.init', _this.question)
      // })
    }
  
    this.refresh = function (_options) {
      this.options = _options
      this._select.refresh(_options)
      _this.question.getElement().find('select').change(function () {
        var selectVal = $(this).val()
  
        var input = null
        if (_this._select._multiple) {
          input = $(this).closest('.rating-container').find('input[name=\'rating_rateandgrade[]\']')
          input.val(JSON.stringify(selectVal))
        } else {
          input = $(this).closest('.rating-container').find('input[name=\'rating_rateandgrade\']')
          input.val(selectVal)
        }
  
        input.trigger('change').trigger('rating.change', [input.val(), ''])
        _this.question.getElement().trigger('question.value.change', _this.question)
      })
      this._autoSelectIfOnlyOneOption(_options)
    }
  
    this.render = function () {
      _this.removeStarRating(_question)
      var _question_element           = _question.getElement()
      var _rating_laboratorio_element = _question_element.find('.rating-laboratorio')
      var _rating_container_element   = _rating_laboratorio_element.find('.rating-container')
      _rating_container_element.removeClass('rating-md')
      _rating_container_element.removeClass('rating-animate')
      _this._select.render(_rating_container_element)
      this._autoSelectIfOnlyOneOption(_options)
    }
  
    this._autoSelectIfOnlyOneOption = function (options) {
      // if(options && Object.keys(options).length === 1) {
      //   this.setRespoonseValue(options[Object.keys(options)[0]].value)
      // }
    }
  
    this.getRespoonseValue = function () {
      // return _this.question.getElement().find('input[name=\'rating_rateandgrade\']').val()
      if (_this._select._multiple) {
        return _this.question.getElement().find('input[name=\'rating_rateandgrade[]\']').val()
      }
      return _this.question.getElement().find('input[name=\'rating_rateandgrade\']').val()
    }
  
    this.enable = function () {
      this._select.enable()
    }
  
    this.disable = function () {
      this._select.disable()
    }
  
    this.setRespoonseValue = function (_value) {
      _this._select.setRespoonseValue(_value)
  
      var input = null
      if (_this._select._multiple) {
        input = _this.question.getElement().find('input[name=\'rating_rateandgrade[]\']')
        input.val(JSON.stringify(_value))
      } else {
        input = _this.question.getElement().find('input[name=\'rating_rateandgrade\']')
        input.val(_value)
      }
      input.trigger('change').trigger('rating.change', [input.val(), ''])
      _this.question.getElement().trigger('question.value.change', _this.question)
  
        // if (_this._select._multiple) {
        //   _input =
        //   _this.question.getElement().find('input[name=\'rating_rateandgrade[]\']').val(JSON.stringify(_value))
        //   _this.question.getElement().find('input[name=\'rating_rateandgrade[]\']').trigger('change', JSON.stringify(_value)) //TODO
        // } else {
        //   _this.question.getElement().find('input[name=\'rating_rateandgrade\']').val(_value)
        //   _this.question.getElement().find('input[name=\'rating_rateandgrade\']').trigger('change', _value) //TODO
        // }
        // _this.question.getElement().find('input[name=\'rating_rateandgrade\']').trigger('change', _value) //TODO
        // _this.question.getElement().trigger('question.value.change', _this.question)
    }
  
    this.removeStarRating = function (_question) {
      var _div_rating_laboratorio = _question.getElement().find('div.rating')
      _div_rating_laboratorio.remove()
    }
  
  }
  
  function ResponseFormatTextarea (_question, _format, _options) {
  
    var _this      = this
    this.question  = _question
    this.options   = _options
    this._textarea = new RgTextArea(_question.id, _this.options)
  
    this.init = function () {
  
      // $('#main').ready(function () {
        _question.getElement().find('fieldset').remove()
        _this.render()
        _question.getElement().find('textarea').change(function (event) {
          _input = _this.question.getElement().find('input[name=\'rating_rateandgrade\']')
          _input.val($(this).val())
          _input.trigger('change').trigger('rating.change', [_input.val(), ''])
          _this.question.getElement().trigger('question.value.change', _this.question)
        })
      // })
    }
  
    this.render = function () {
      _this.removeStarRating(_question)
      var _question_element           = _question.getElement()
      var _rating_laboratorio_element = _question_element.find('.rating-laboratorio')
      var _rating_container_element   = _rating_laboratorio_element.find('.rating-container')
      _rating_container_element.removeClass('rating-md')
      _rating_container_element.removeClass('rating-animate')
      _this._textarea.render(_rating_container_element)
    }
  
    this.removeStarRating = function (_question) {
      var _div_rating_laboratorio = _question.getElement().find('div.rating')
      _div_rating_laboratorio.remove()
    }
  
    this.getRespoonseValue = function () {
      return _this.question.getElement().find('input[name=\'rating_rateandgrade\']').val()
    }
  
    this.enable = function () {
      this._textarea.enable()
    }
  
    this.disable = function () {
      this._textarea.disable()
    }
  
    this.setRespoonseValue = function (_value) {
      // console.log(_value);
      _this.question.getElement().find('input[name=\'rating_rateandgrade\']').val(_value)
      $('.input-group.input-formulario#input_' + _this.question.id + ' textarea').val(_value)
      _this.question.getElement().find('input[name=\'rating_rateandgrade\']').trigger('change', _value) //TODO
      _this.question.getElement().trigger('question.value.change', _this.question)
    }
  
    this.accesibility = function (tabindex) {
      this.question.getElement().find('textarea').each(function () {
        $(this).attr('tabindex', tabindex)
        tabindex += 1
      })
      return tabindex
    }
  
  }
  
  function ResponseFormatInputText (_question, _format, _options) {
  
    var _this     = this
    this.question = _question
    this.options  = _options
    this._text    = new RgInputText(_question.id, _this.options)
  
    this.init = function () {
      // $('#main').ready(function () {
        _question.getElement().find('fieldset').remove()
        _this.render()
        _question.getElement().find('input[name="' + _this.question.id + '"]').change(function (event) {
          _input = _this.question.getElement().find('input[name=\'rating_rateandgrade\']')
          _input.val($(this).val())
          _input.trigger('change').trigger('rating.change', [_input.val(), ''])
          _this.question.getElement().trigger('question.value.change', _this.question)
        })
      // })
    }
  
    this.render = function () {
      _this.removeStarRating(_question)
      var _question_element           = _question.getElement()
      var _rating_laboratorio_element = _question_element.find('.rating-laboratorio')
      var _rating_container_element   = _rating_laboratorio_element.find('.rating-container')
      _rating_container_element.removeClass('rating-md')
      _rating_container_element.removeClass('rating-animate')
      _this._text.render(_rating_container_element)
    }
  
    this.removeStarRating = function (_question) {
      var _div_rating_laboratorio = _question.getElement().find('div.rating')
      _div_rating_laboratorio.remove()
    }
  
    this.getRespoonseValue = function () {
      return _this.question.getElement().find('input[name=\'rating_rateandgrade\']').val()
    }
  
    this.setRespoonseValue = function (_value) {
      _this.question.getElement().find('input[name=\'rating_rateandgrade\']').val(_value)
      _this.question.getElement().find('input[name=\'rating_rateandgrade\']').trigger('change', _value) //TODO
      _this.question.getElement().find('input[type=\'text\']').val(_value)
      _this.question.getElement().trigger('question.value.change', _this.question)
    }
  
  }
  
  function ResponseFormatInputDate (_question, _format, _options) {
  
    var _this     = this
    this.question = _question
    this.options  = _options
    this._date    = new RgInputDate(_question.id, _this.options)
  
    this.init = function () {
      // $('#main').ready(function () {
        _question.getElement().find('fieldset').remove()
        _this.render()
        // console.log(_question.getElement());
        _question.getElement().find('input[name="' + _this.question.id + '"]').change(function (event) {
          _input = _this.question.getElement().find('input[name=\'rating_rateandgrade\']')
          _input.val($(this).val())
          _input.trigger('change').trigger('rating.change', [_input.val(), ''])
          _this.question.getElement().trigger('question.value.change', _this.question)
        })
      // })
    }
  
    this.render = function () {
      _this.removeStarRating(_question)
      var _question_element           = _question.getElement()
      var _rating_laboratorio_element = _question_element.find('.rating-laboratorio')
      var _rating_container_element   = _rating_laboratorio_element.find('.rating-container')
      _rating_container_element.removeClass('rating-md')
      _rating_container_element.removeClass('rating-animate')
      _this._date.render(_rating_container_element)
    }
  
    this.removeStarRating = function (_question) {
      var _div_rating_laboratorio = _question.getElement().find('div.rating')
      _div_rating_laboratorio.remove()
    }
  
    this.getRespoonseValue = function () {
      return _this.question.getElement().find('input[name=\'rating_rateandgrade\']').val()
    }
  
    this.setRespoonseValue = function (_value) {
      _this.question.getElement().find('input[name=\'rating_rateandgrade\']').val(_value)
      _this.question.getElement().find('input[name=\'rating_rateandgrade\']').trigger('change', _value) //TODO
      _this.question.getElement().find('input[type=\'text\']').val(_value)
      _this.question.getElement().trigger('question.value.change', _this.question)
    }
  
  }
  
  function ResponseFormatFileImages (_question, _format, _options, _format_options) {
  
    var _this     = this
    this.question = _question
    this.options  = _options
    this._image   = new RgInputFileImage(_question.id, _this.options, _format_options)
  
    this.init = function () {
  
      // $('#main').ready(function () {
        _question.getElement().find('fieldset').remove()
        _this.render()
  
        if (_this._image._multiple) {
          var rating = _this.question.getElement().find('input[name=\'rating_rateandgrade\']')
          rating.attr('name', 'rating_rateandgrade[]')
        }
  
        var input = null
        _question.getElement().find('input[type="file"]').change(function (e) {
  
          if (_this._image._multiple) {
            input = $(this).closest('.rating-container').find('input[name=\'rating_rateandgrade[]\']')
            input.val($(this).val())
          }else{
            input = _this.question.getElement().find('input[name=\'rating_rateandgrade\']')
            input.val($(this).val())
          }
          input.trigger('change').trigger('rating.change', [input.val(), ''])
          _this.question.getElement().trigger('question.value.change', _this.question)
        })
      // })
    }
  
    this.render = function () {
      _this.removeStarRating(_question)
      var _question_element           = _question.getElement()
      var _rating_laboratorio_element = _question_element.find('.rating-laboratorio')
      var _rating_container_element   = _rating_laboratorio_element.find('.rating-container')
      _rating_container_element.removeClass('rating-md')
      _rating_container_element.removeClass('rating-animate')
      _this._image.render(_rating_container_element)
    }
  
    this.getRespoonseValue = function () {
      if (_this._image._multiple) {
        return _this.question.getElement().find('input[name=\'rating_rateandgrade[]\']').val()
      }
      return _this.question.getElement().find('input[name=\'rating_rateandgrade\']').val()
    }
  
    this.removeStarRating = function (_question) {
      var _div_rating_laboratorio = _question.getElement().find('div.rating')
      _div_rating_laboratorio.remove()
    }
  
    this.enable = function () {
      var divImageFile = _this.question.getElement().find('div.imageFile')
      if (divImageFile.length > 0) {
        _this.question.getElement().find('.imageFile').remove()
      }
      this._image.enable()
    }
  
    this.disable = function () {
      this._image.disable()
    }
  
    this.setRespoonseValue = function (_value) {
      //TODO MULTIPLE
  
      _this.question.getElement().find('input[name=\'rating_rateandgrade\']').val(_value)
      // _this.question.getElement().find('input[name=\'rating_rateandgrade\']').trigger('change', _value) //TODO
  
      var divImageFile = _this.question.getElement().find('div.imageFile')
      if (divImageFile.length === 0) {
        divImageFile = _this.question.getElement().append('<div class="imageFile">').find('.imageFile')
      }
      divImageFile.empty()
  
      if (_value) {
        divImageFile.append('<img src="' + _value + '" alt="input_image" class="input_img" height="100">')
      }
  
      _this.question.getElement().trigger('question.value.change', _this.question)
    }
  }
  
  function ResponseFormatNumericInput (_question, _options) {
  
    var _this          = this
    this.question      = _question
    this._defaultValue = 0
    this._decimal      = false
  
    if (typeof _options !== 'undefined') {
  
      if (typeof _options['defaultValue'] !== 'undefined') {
        this._defaultValue = _options['defaultValue']
      }
  
      if (typeof _options['decimal'] !== 'undefined') {
        this._decimal = _options['decimal']
      }
    }
  
    this._numeric = new RgInputNumeric(_question.id, _options)
  
    this.init = function () {
      // $('#main').ready(function () {
        _question.getElement().find('fieldset').remove()
        _this.render()
        _question.getElement().find('input[name="rating_rateandgrade"]').val(_this._defaultValue)
        _question.getElement().find('input.ui-spinner-input').bind('change', function () {
  
          if ($(this).data('onInputPrevented')) return
          var _error_msg = ''
          var _val       = $(this).val()
          var _max       = $(this).spinner('option', 'max')
          var _min       = $(this).spinner('option', 'min')
  
          if (!_this._decimal) {
            if ($(this).val()) {
              _val = parseInt($(this).val())
              if ($(this).val() != _val) {
                _error_msg += 'Introduzca un Número Entero sin Decimales \n'
              }
              $(this).val(_val)
            }
          }
          if (typeof _val == 'undefined' || _val == null || (_val != 0 && isNaN(_val))) {
            _val = _this._defaultValue
            _error_msg += 'Valor Númerico Incorrecto'
          }
          if (_error_msg != '') {
            alert(_error_msg)
          }
  
          // _val = _val > _max ? _max : _val < _min ? _min : _val
          console.log(_val)
  
          $(this).spinner('value', _val)
  
          _input = $(this).closest('.rating-container').find('input[name=\'rating_rateandgrade\']')
          _input.val($(this).val())
          _input.trigger('change').trigger('rating.change', [_input.val(), ''])
          _this.question.getElement().trigger('question.value.change', _this.question)
  
        })
        _question.getElement().find('a.ui-button').on('click', function () {
          $(this).focus()
          // _input = _this.question.getElement().find("input[name='rating_rateandgrade']");
          _input = $(this).closest('.rating-container').find('input[name=\'rating_rateandgrade\']')
          _input.val(_this.question.getElement().find('input.ui-spinner-input').val())
          _input.trigger('change').trigger('rating.change', [_input.val(), ''])
          _this.question.getElement().trigger('question.value.change', _this.question)
        })
      // })
    }
  
    this.render = function () {
      _this.removeStarRating(_question)
      var _question_element           = _question.getElement()
      var _rating_laboratorio_element = _question_element.find('.rating-laboratorio')
      var _rating_container_element   = _rating_laboratorio_element.find('.rating-container')
      _rating_container_element.removeClass('rating-md')
      _rating_container_element.removeClass('rating-animate')
      _this._numeric.render(_rating_container_element)
    }
  
    this.removeStarRating = function (_question) {
      var _div_rating_laboratorio = _question.getElement().find('div.rating')
      _div_rating_laboratorio.remove()
    }
  
    this.getRespoonseValue = function () {
      return _this.question.getElement().find('input[name=\'rating_rateandgrade\']').val()
    }
  
    this.enable = function () {
      _this._numeric.enable()
    }
  
    this.disable = function () {
      _this._numeric.disable()
    }
  
    this.setRespoonseValue = function (_value) {
      _this.question.getElement().find('input[name=\'rating_rateandgrade\']').val(_value)
      _this.question.getElement().find('input[type=\'number\']').val(_value)
      _this.question.getElement().find('input[name=\'rating_rateandgrade\']').trigger('change', _value) //TODO
      _this.question.getElement().trigger('question.value.change', _this.question)
    }
  
  }
  
  function ResponseFormatSignature (_question, _options) {
  
    var _this          = this
    this.question      = _question
  
    this._signature_pad = new RgSignature(_question.id, _options)
  
    this.init = function () {
      _this.render()
      this._signature_pad.get_signature_pad().addEventListener("endStroke", () => {
        _input = _this.question.getElement().find('input[name=\'rating_rateandgrade\']')
        _input.val(this._signature_pad.get_signature_pad().toDataURL())
        _input.trigger('change').trigger('rating.change', [_input.val(), ''])
        _this.question.getElement().trigger('question.value.change', _this.question)
      });
      _question.getElement().find('#clear-signature').on('click', function () {
        let _input = $(this).closest('.grupo-pregunta._rg_format_signature').find('input[name=\'rating_rateandgrade\']')
        _this._signature_pad.clean()
        _input.val('')
        _input.trigger('change').trigger('rating.change', [_input.val(), ''])
      })
    }
  
    this.render = function () {
      _this.removeStarRating(_question)
      var _question_element           = _question.getElement()
      var _rating_laboratorio_element = _question_element.find('.rating-laboratorio')
      var _rating_container_element   = _rating_laboratorio_element.find('.rating-container')
      _rating_container_element.removeClass('rating-md')
      _rating_container_element.removeClass('rating-animate')
      _this._signature_pad.render(_rating_container_element)
    }
  
    this.removeStarRating = function (_question) {
      var _div_rating_laboratorio = _question.getElement().find('div.rating')
      _div_rating_laboratorio.remove()
    }
  
    this.getRespoonseValue = function () {
      return this._signature_pad.get_signature_pad().toDataURL()
    }
  
    this.enable = function () {
    }
  
    this.disable = function () {
    }
  
    this.setRespoonseValue = function (_value) {
  
    }
  
  }
  
  // RATEANDGRADE INPUT COMPONENTS
  
  function RgRadioGroup (_data_id, _options) {
  
    var _this     = this
    this._data_id = _data_id
    this.options  = _options
  
    this.radiogroup_html = '<div><fieldset>' +
      '<div id="radiogroup-{{dataId}}" class="radiogroup" data-id="{{dataId}}">' +
      '<div class="radiogroup-body" data-id="{{dataId}}">' +
      '{{body}}' +
      '</div>' +
      '</div>' +
      '</fieldset></div>'
  
    this.radiobutton_html = '<div class="radiogroup-element">' +
      '<input type="radio" name="radio-{{dataId}}" id="radio-{{dataId}}-{{value}}" class="radio-{{dataId}}" value="{{value}}">' +
      '<label for="radio-{{dataId}}-{{value}}">' +
      '<span class="label-text">{{label}}</span>' +
      '</label>' +
      '</div>'
  
    this.getOutput = function () {
  
      var _output_html      = _this.radiogroup_html.split('{{dataId}}').join(_data_id)
      var _output_body_html = ''
  
      for (var _radio in _this.options) {
        var _output_radio_element = _this.radiobutton_html.split('{{dataId}}').join(_data_id)
        _output_radio_element     = _output_radio_element.split('{{value}}').join(_this.options[_radio]['value'])
        _output_radio_element     = _output_radio_element.split('{{label}}').join(_this.options[_radio]['label'])
        _output_body_html         = _output_body_html + _output_radio_element
      }
      return _output_html.replace('{{body}}', _output_body_html)
    }
  
    this.render = function (_parent_selector) {
      $(_parent_selector).append(_this.getOutput())
      this.init()
    }
  
    this._getIcon = function () {
      var icon = false
  
      if (typeof _this.options[1] != 'undefined') {
        if (typeof _this.options[1]['icon'] != 'undefined' && _this.options[1]['icon']) {
          icon = true
        }
      }
      return icon
    }
  
    this.init = function () {
      $('#radiogroup-' + _this._data_id).find('input[type="radio"]').checkboxradio({
                                                                                     icon: this._getIcon()
                                                                                   })
    }
  
    this.setRespoonseValue = function (_value) {
      $('.rating-laboratorio[data-id=' + _this._data_id + '] div#radiogroup-' + _this._data_id + ' input').each(function (index, element) {
        $(element).checkboxradio('destroy')
        if (_value == $(element).val()) {
          $(element).prop('checked', true)
        } else {
          $(element).prop('checked', false)
        }
        _this.init()
      })
    }
  
    this.enable = function () {
      $('#radiogroup-' + _this._data_id).find('input[type="radio"]').checkboxradio('enable')
    }
  
    this.disable = function () {
      $('#radiogroup-' + _this._data_id).find('input[type="radio"]').checkboxradio('disable')
    }
  
  }
  
  function RgRadioGroupthisandthat (_data_id, _options) {
  
    var _this     = this
    this._data_id = _data_id
    this.options  = _options
  
    this.radiogroup_html = '<div id="control-group"><fieldset>' +
      '<div id="radiogroup-{{dataId}}" class="radiogroup" data-id="{{dataId}}">' +
      '<div class="radiogroup-body" data-id="{{dataId}}">' +
      '<ul>' +
      '{{body}}' +
      '</ul>' +
      '</div>' +
      '</div>' +
      '</fieldset></div>'
  
    this.radiobutton_html = '<li class="list-ass">' +
      '<div class="radiogroup-element">' +
      '<input type="radio" name="radio-{{dataId}}" id="radio-{{dataId}}-{{value}}" class="radio-{{dataId}}" value="{{value}}">' +
      '<label for="radio-{{dataId}}-{{value}}">' +
      '<div class="img-div"><img alt="{{label}}" id="img-radio-{{dataId}}-{{value}}" class="image-checkbox" src="{{image}}"></div>' +
      '<div class="label-text"><p>{{label}}</p></div>' +
      '</label>' +
      '</div>' +
      '</li>'
  
    this.getOutput = function () {
  
      var _output_html      = _this.radiogroup_html.split('{{dataId}}').join(_data_id)
      var _output_body_html = ''
  
      for (var _radio in _this.options) {
        var _output_radio_element = _this.radiobutton_html.split('{{dataId}}').join(_data_id)
        _output_radio_element     = _output_radio_element.split('{{value}}').join(_this.options[_radio]['value'])
        _output_radio_element     = _output_radio_element.split('{{label}}').join(_this.options[_radio]['label'])
        _output_radio_element     = _output_radio_element.split('{{image}}').join(_this.options[_radio]['image'])
        _output_body_html         = _output_body_html + _output_radio_element
      }
      return _output_html.replace('{{body}}', _output_body_html)
    }
  
    this.render = function (_parent_selector) {
      $(_parent_selector).append(_this.getOutput())
      this.init()
    }
  
    this.init = function () {
      $('#control-group').controlgroup({
                                         'direction': 'vertical'
                                       })
      $('#radiogroup-' + _this._data_id).find('input[type="radio"]').checkboxradio({
                                                                                     icon: false
                                                                                   })
    }
  
    this.setRespoonseValue = function (_value) {
      $('.rating-laboratorio[data-id=' + _this._data_id + '] div#radiogroup-' + _this._data_id + ' input').each(function (index, element) {
        $(element).checkboxradio('destroy')
        if (_value == $(element).val()) {
          $(element).prop('checked', true)
        }
        _this.init()
      })
    }
  }
  
  function RgInputDate (_data_id, _options) {
  
    var _this         = this
    this._data_id     = _data_id
    this._min         = ''
    this._max         = ''
    this._defaultDate = ''
    this._disableDays = ''
    this._lang = 'es-ES'
    this._postion     = false
    this._image       = {}
    this._text        = {}
    this._format      = {}
    this._placeholder = null
    this._dateFormat  = 'dd-mm-yy'
  
  
    if (typeof _options['minDate'] != 'undefined') {
      this._min = _options['minDate']
    }
    if (typeof _options['maxDate'] != 'undefined') {
      this._max = _options['maxDate']
    }
    if (typeof _options['defaultDate'] != 'undefined') {
      this._defaultDate = _options['defaultDate']
    }
    if (typeof _options['position'] != 'undefined') {
      this._position =  { beforeShow: function (textbox, instance) {
          var txtBoxOffset = $(this).offset();
          var top = txtBoxOffset.top;
          var left = txtBoxOffset.left;
          var textBoxHeight = $(this).outerHeight();
          setTimeout(function () {
            instance.dpDiv.css({
              top: top - 205, //you can adjust this value accordingly
              left: left//show at the end of textBox
            });
          }, 0);
        }
      }
    }
    if (typeof _options['noweekends'] != 'undefined') {
      this._disableDays = $.datepicker.noWeekends
    }
  
    this._disableDaysHolidaysAndWeekends = function (date) {
      var unavailableDates = _options['holidays']
      dmy = date.getDate() + "-" + (date.getMonth()+1) + "-" + date.getFullYear();
      if ($.inArray(dmy, unavailableDates) < 0) {
        return $.datepicker.noWeekends(date) // weekends no habilitados
      } else {
        return [false,"",""];
      }
    }
  
    if (typeof _options['holidays'] != 'undefined') {
      this._disableDays = this._disableDaysHolidaysAndWeekends
    }
  
    if (typeof _options['lang'] != 'undefined') {
      this._lang = _options['lang']
    }
  
    if (typeof _options['font-awesome'] != 'undefined' && _options['font-awesome']) {
      this._text = {
        showOn:     'button',
        buttonText: '<i class=\'fa fa-calendar\'></i>'
      }
    }
    if (typeof _options['img'] != 'undefined') {
      this._image = {
        showOn:          'button',
        buttonImage:     _options['img'],
        buttonImageOnly: true,
        buttonText:      'Select date'
      }
    }
    if (typeof _options['placeholder'] != 'undefined') {
      this._placeholder = _options['placeholder']
    }
    if (typeof _options['dateFormat'] != 'undefined') {
      this._dateFormat = _options['dateFormat']
    }
  
    this.getOutput = function () {
  
      var _required_markup = ''
      if (_options['required']) {
        _required_markup = '<span class="fb-required">*</span></label>'
      }
  
      var _required_label = ''
      if (_options['display_label_required']) {
        if (_options['required']) {
          _required_label = '<label class="fb-text-label desc">[' + _options['label_required'] + ']</label>'
        }
      }
  
      return '' +
        '<div class="fb-text form-group field-' + _this._data_id + '">' +
        '<div class="input-group input-formulario" id="date-' + _this._data_id + '">' +
        '   <input type="text" value="' + _this._defaultDate + '" class="form-control input-formulario" placeholder="' + _this._placeholder + '" name="' + _this._data_id + '" id="' + _this._data_id + '">' +
        '</div>' +
        '</div>'
    }
  
    this.render = function (_parent_selector) {
      $(_parent_selector).append(_this.getOutput())
      this.init()
    }
  
    this.init = function () {
  
      if(this._lang == 'es-ES') {
        _this._format = Object.assign({
          dateFormat: _this._dateFormat,
          minDate: _this._min,
          maxDate: _this._max,
          monthNames: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'],
          monthNamesShort: ['Ene', 'Feb', 'Mar', 'Abr', 'May', 'Jun', 'Jul', 'Ago', 'Sep', 'Oct', 'Nov', 'Dic'],
          dayNamesMin: ['Dom', 'Lun', 'Mar', 'Mie', 'Jue', 'Vie', 'Sab'],
          beforeShowDay: this._disableDays,
        }, _this._image)
      }
      if(this._lang == 'pt-PT') {
        _this._format = Object.assign({
          dateFormat: _this._dateFormat,
          minDate: _this._min,
          maxDate: _this._max,
          monthNames: ['Janeiro', 'Fevereiro', 'Março', 'Abril', 'Maio', 'Junho', 'Julho', 'Agosto', 'Setembro', 'Outubro', 'Novembro', 'Dezembro'],
          monthNamesShort: ['Jan', 'Fev', 'Mar', 'Abr', 'Mai', 'Jun', 'Jul', 'Ago', 'Set', 'Out', 'Nov', 'Dez'],
          dayNamesMin: ['Dom', 'Seg', 'Ter', 'Qua', 'Qui', 'Sex', 'Sáb', 'Dom'],
          beforeShowDay: this._disableDays,
        }, _this._image)
      }
  
      _this._format = Object.assign(_this._format, _this._text)
      _this._format = Object.assign(_this._format, _this._position)
  
      $('#date-' + _this._data_id).find('input[type="text"]').datepicker(_this._format)
    }
  
    this.setRespoonseValue = function (_value) {
    }
  }
  
  function RgCheckboxGroup (_data_id, _options) {
  
    var _this     = this
    this._data_id = _data_id
    this.options  = _options
  
    this.checkboxgroup_html = '<div><fieldset>' +
      '<div id="checkboxgroup-{{dataId}}" class="checkboxgroup" data-id="{{dataId}}">' +
      '<div class="checkboxgroup-body" data-id="{{dataId}}">' +
      '{{body}}' +
      '</div>' +
      '</div>' +
      '</fieldset></div>'
  
    this.checkboxbutton_html = '<div class="checkboxgroup-element">' +
      '<input type="checkbox" name="checkbox-{{dataId}}" id="checkbox-{{dataId}}-{{value}}" class="checkbox-{{dataId}}" value="{{value}}">' +
      '<label for="checkbox-{{dataId}}-{{value}}">' +
      '<span class="label-text">{{label}}</span>' +
      '</label>' +
      '</div>'
  
    this.getOutput = function () {
  
      var _output_html      = _this.checkboxgroup_html.split('{{dataId}}').join(_data_id)
      var _output_body_html = ''
      var cont              = 0
      for (var _checkbox in _this.options) {
        cont                         = cont + 1
        var _output_checkbox_element = _this.checkboxbutton_html.split('{{dataId}}').join(_data_id)
        _output_checkbox_element     = _output_checkbox_element.split('{{value}}').join(cont)
        _output_checkbox_element     = _output_checkbox_element.split('{{label}}').join(_this.options[_checkbox]['label'])
        _output_body_html            = _output_body_html + _output_checkbox_element
      }
      return _output_html.replace('{{body}}', _output_body_html)
    }
  
    this.render = function (_parent_selector) {
      $(_parent_selector).append(_this.getOutput())
      this.init()
    }
  
    this.init = function () {
      $('#checkboxgroup-' + _this._data_id).find('input[type="checkbox"]').checkboxradio({
                                                                                           icon: true
                                                                                         })
    }
  
    this.setRespoonseValue = function (_value) {
      // console.log("checkBoxGroup.setResponseValue: "+_value);
      $('.rating-laboratorio[data-id=' + _this._data_id + '] div#checkboxgroup-' + _this._data_id + ' input').each(function (index, element) {
        $(element).checkboxradio('destroy')
        if (Array.isArray(_value)) {
          _value.forEach(function (_valueResponse) {
            if (_valueResponse == $(element).val()) {
              $(element).prop('checked', true)
            }
          })
        } else {
          if (_value == $(element).val()) {
            $(element).prop('checked', true)
          }
        }
        _this.init()
      })
    }
  }
  
  function RgCheckboxSimple (_data_id, _options) {
  
    var _this     = this
    this._data_id = _data_id
    this.options  = _options
  
    this.checkboxgroup_html = '<div><fieldset>' +
        '<div id="checkboxgroup-{{dataId}}" class="checkboxgroup" data-id="{{dataId}}">' +
        '<div class="checkboxgroup-body" data-id="{{dataId}}">' +
        '{{body}}' +
        '</div>' +
        '</div>' +
        '</fieldset></div>'
  
    this.checkboxbutton_html = '<div class="checkboxgroup-element">' +
        '<input type="checkbox" name="checkbox-{{dataId}}" id="checkbox-{{dataId}}-{{value}}" class="checkbox-{{dataId}}" value="{{value}}">' +
        '<label for="checkbox-{{dataId}}-{{value}}">' +
        '<span class="label-text">{{label}}</span>' +
        '</label>' +
        '</div>'
  
    this.getOutput = function () {
  
      var _output_html      = _this.checkboxgroup_html.split('{{dataId}}').join(_data_id)
      var _output_body_html = ''
      var cont              = 0
      for (var _checkbox in _this.options) {
        cont                         = cont + 1
        var _output_checkbox_element = _this.checkboxbutton_html.split('{{dataId}}').join(_data_id)
        _output_checkbox_element     = _output_checkbox_element.split('{{value}}').join(cont)
        _output_checkbox_element     = _output_checkbox_element.split('{{label}}').join(_this.options[_checkbox]['label'])
        _output_body_html            = _output_body_html + _output_checkbox_element
      }
      return _output_html.replace('{{body}}', _output_body_html)
    }
  
    this.render = function (_parent_selector) {
      $(_parent_selector).append(_this.getOutput())
    }
  
    this.setRespoonseValue = function (_value) {
      $('.rating-laboratorio[data-id=' + _this._data_id + '] input').each(function (index, element) {
        if (Array.isArray(_value)) {
          _value.forEach(function (_valueResponse) {
            if (_valueResponse == $(element).val()) {
              $(element).prop('checked', true)
            }
          })
        } else {
          if (_value == $(element).val()) {
            $(element).prop('checked', true)
          }
        }
      })
    }
  }
  
  function RgCheckboxGroupthisanthat (_data_id, _options) {
  
    var _this     = this
    this._data_id = _data_id
    this.options  = _options
    this._parent  = null
  
    this.checkboxgroup_html = '<div id="control-group"><fieldset>' +
      '<div id="checkboxgroup-{{dataId}}" class="checkboxgroup" data-id="{{dataId}}">' +
      '<div class="checkboxgroup-body" data-id="{{dataId}}">' +
      '<ul>' +
      '{{body}}' +
      '</ul>' +
      '</div>' +
      '</div>' +
      '</fieldset></div>'
  
    this.checkboxbutton_html = '<li class="list-ass">' +
      '<div class="checkboxgroup-element">' +
      '<input type="checkbox" name="checkbox-{{dataId}}" id="checkbox-{{dataId}}-{{value}}" class="checkbox-{{dataId}}" value="{{value}}">' +
      '<label for="checkbox-{{dataId}}-{{value}}">' +
      '<div class="img-div"><img id="img-checkbox-{{dataId}}-{{value}}" class="image-checkbox" src="{{image}}"></div>' +
      '<div class="label-text"><p>{{label}}</p></div>' +
      '</label>' +
      '</div>' +
      '</li>'
  
    this.getOutput = function () {
  
      var _output_html      = _this.checkboxgroup_html.split('{{dataId}}').join(_data_id)
      var _output_body_html = ''
      var cont              = 0
      for (var _checkbox in _this.options) {
        cont                         = cont + 1
        var _output_checkbox_element = _this.checkboxbutton_html.split('{{dataId}}').join(_data_id)
        _output_checkbox_element     = _output_checkbox_element.split('{{value}}').join(cont)
        _output_checkbox_element     = _output_checkbox_element.split('{{label}}').join(_this.options[_checkbox]['label'])
        _output_checkbox_element     = _output_checkbox_element.split('{{image}}').join(_this.options[_checkbox]['image'])
        _output_body_html            = _output_body_html + _output_checkbox_element
      }
      return _output_html.replace('{{body}}', _output_body_html)
    }
  
    this.render = function (_parent_selector) {
  
      _this._parent = $(_parent_selector)
      _this._parent.append(_this.getOutput())
      this.init()
  
    }
  
    this.init = function () {
      _this._parent.find('#checkboxgroup-' + _this._data_id).find('input[type="checkbox"]').checkboxradio({
                                                                                                            icon: false
                                                                                                          })
    }
  
    this.setRespoonseValue = function (_value) {
  
      _this._parent.find('div#checkboxgroup-' + _this._data_id + ' input').each(function (index, element) {
  
        $(element).checkboxradio('destroy')
        if (Array.isArray(_value)) {
          _value.forEach(function (_valueResponse) {
            if (_valueResponse == $(element).val()) {
              $(element).prop('checked', true)
            }
          })
        } else {
          if (_value == $(element).val()) {
            $(element).prop('checked', true)
          }
        }
  
      })
  
      _this.init()
    }
  }
  
  function RgSelect (_data_id, _options, _format_options) {
  
    var _this             = this
    this._data_id         = _data_id
    this._options         = _options
    this._format_options  = _format_options
    this._unselect_text   = '- Seleccione una opción -'
    this._default_option  = true
    this._enable_chosen   = false
    // this._enable_select2   = false
    this._enable_select2   =  _format_options && _format_options.enable_select2 ? true : false
    this._max_selected_options   = false
    this._src = ""
    this._fnTemplate      = function (data, container) {
      var $result = $("<span></span>");
      $result.text(data.text);
      return $result;
    }
    this._parent_selector = null
    this._disabled        = false
    this._multiple        = false
    this._class           = ''
  
    this.render = function (_parent_selector) {
      this._parent_selector = _parent_selector
      this.setFormatOptions(this._format_options)
      $(_parent_selector).append(this.getOutput())
      this.init()
    }
  
    this.setFormatOptions = function (_format_options) {
      if (_format_options) {
  
        this.format_options = JSON.parse(JSON.stringify(_format_options))
  
        if (this.format_options.default_option_select === 0) {
          this._default_option = false
        }
  
        if (this.format_options.default_option_label) {
          this._unselect_text = this.format_options.default_option_label
        }
  
        if (this.format_options.enable_chosen) {
          this._enable_chosen = true
        }
  
        if (this.format_options.enable_select2) {
          this._enable_select2 = true
        }
  
        if (this.format_options.disabled) {
          this._disabled = 'disabled'
        }
  
        if (this.format_options.class) {
          this._class = this.format_options.class
        }
  
        if (this.format_options.multiple) {
          this._multiple = 'multiple'
        }
  
        if (this.format_options.max_selected_options) {
          this._max_selected_options = this.format_options.max_selected_options
        }
  
        if (this.format_options.src) {
          this._src = this.format_options.src
        }
  
        const _this = this
        if (this.format_options.templateImage) {
          this._fnTemplate = function (value) {
            if(value.title) {
              return $("<span><img class='imageSelect' src='" + _this._src + value.title + "'/><span class='textSelect'>" + value.text + "</span></span>");
            }else{
              return $("<span><span class='textSelect'>" + value.text + "</span></span>");
            }
          }
        }
  
      }
  
    }
  
    this.getOutput = function () {
  
      var _select_html      = '' + '<fieldset>' + '<div id="select-{{dataId}}" class="select-wrap" data-id="{{dataId}}">' + '<div class="select-body" data-id="{{dataId}}">' + '<select ' + _this._disabled + ' ' + _this._multiple + ' name="select-{{dataId}}" id="select-{{dataId}}" class="select ' + _this._class + '" data-id="{{dataId}}">' + '{{body}}' + '</select>' + '</div>' + '</div>' + '</fieldset>'
      var _output_html      = _select_html.split('{{dataId}}').join(this._data_id)
      var _output_body_html = ''
  
      if (this._default_option) {
        _output_body_html = _output_body_html + '<option value>' + this._unselect_text + '</option>'
      }
  
      for (var _option in this._options) {
        _output_body_html += this.getOptionOutput(_option)
      }
  
      return _output_html.replace('{{body}}', _output_body_html)
    }
  
    this.getOptionOutput = function (_option) {
      var _option_html           = '<option title="{{title}}" value="{{value}}">{{label}}</option>'
      var _output_option_element = _option_html.split('{{dataId}}').join(this._data_id)
      _output_option_element     = _output_option_element.split('{{value}}').join(this._options[_option]['value'])
      _output_option_element     = _output_option_element.split('{{label}}').join(this._options[_option]['label'])
      if(this._options[_option]['title']) {
        _output_option_element = _output_option_element.split('{{title}}').join(this._options[_option]['title'])
      }else{
        _output_option_element = _output_option_element.split('{{title}}').join("")
      }
      return _output_option_element
    }
  
    this.init = function () {
  
      if (this._enable_select2) {
        this.select2Settigns()
      }
  
      if (this._enable_chosen) {
        this.chosenSettigns()
      }
  
    }
  
    this.chosenSettigns = function () {
      var _element = this._parent_selector.find('select[data-id="' + this._data_id + '"]')
      _element.chosen({
                        allow_single_deselect: true,
                        max_selected_options : this._max_selected_options,
                        placeholder_text_multiple : this._unselect_text,
                        // max_shown_results:     100
                      })
  
      // _element.on('chosen:showing_dropdown', function() {
      //   _element.parent().find('.chosen-search-input').prop('focus', false).blur();
      // })
      //
      // _element.on('chosen:hiding_dropdown', function() {
      //   _element.parent().find('.chosen-search-input').attr('display','none');
      // })
      //
      // _element.on('chosen:activate', function() {
      //   _element.parent().find('.chosen-search-input').prop('focus', false).blur();
      // })
  
      var _container = this._parent_selector.find('chosen-container')
      _container.ready(function () {
        $('.chosen-container').on('touchstart', function (e) {
          $(this).trigger('mousedown')
        })
      })
    }
  
    this.select2Settigns = function () {
      var _element = this._parent_selector.find('select[data-id="' + this._data_id + '"]')
      _element.select2({
                        maximumSelectionLength : this._max_selected_options,
                        placeholder: this._unselect_text,
                        allowClear: true,
                        dropdownParent: this._parent_selector,
                        // debug:true,
                        // language: {
                        //   maximumSelected: function (e) {
                        //     return "Solo puedes seleccionar un máximo de " + e.maximum + " opciones";
                        //   },
                        //   minimumSelected: function (e) {
                        //     return "Tienes que seleccionar un mínimo de " + e.minimum + " opciones";
                        //   },
                        //   noResults: function (){
                        //     return "No se han encontrado resultados";
                        //   }
                        // }
                        // ,
                        templateResult: this._fnTemplate,
                        templateSelection: this._fnTemplate
      })
  
      var _this = this
      _element.on('select2:open', function() {
        // $('.select2-search__field').prop('focus', false);
        // _this._parent_selector.find('.select2-search__field').prop('focus', false);
        // _this._parent_selector.find('.select2-results').focus();
      })
  
      _element.on('select2:closing', function() {
        // $('.select2-search__field').prop('focus', true);
        // _this._parent_selector.find('.select2-search__field').prop('focus', true);
      })
  
    }
  
    this.refresh = function (_options) {
      if (_this._parent_selector) {
        var _element = this._parent_selector.find('select[data-id=' + _this._data_id + ']')
        if (this._enable_chosen) {
          _element.chosen('destroy')
        }
        if (this._enable_select2) {
          if (_element.hasClass("select2-hidden-accessible")) {
            _element.select2('destroy')
          }
        }
        $(_this._parent_selector).find('fieldset').remove()
        _this._options = _options
        _this.render(_this._parent_selector)
      }
    }
  
    this.setRespoonseValue = function (_value) {
      if (!this._parent_selector) {
        this._parent_selector = $('body .tab-content .panel .grupo-pregunta[data-id=' + _this._data_id + '] .rating-container')
      }
  
      var _element = this._parent_selector.find('select[data-id=' + _this._data_id + ']')
      _element.val(_value)
      if (this._enable_chosen) {
        _element.trigger('chosen:updated')
      }
      if (this._enable_select2) {
        _element.trigger('change.select2')
      }
      // _this.init()
    }
  
    this.enable = function () {
      var select = this._parent_selector.find('select[data-id=' + _this._data_id + ']')
      select.attr('disabled', false)
      if (this._enable_chosen) {
        select.trigger('chosen:updated')
      }
      if (this._enable_select2) {
        select.trigger('change.select2')
      }
    }
  
    this.disable = function () {
      var select = this._parent_selector.find('select[data-id=' + _this._data_id + ']')
      select.attr('disabled', 'disabled')
      if (this._enable_chosen) {
        select.trigger('chosen:updated')
      }
      if (this._enable_select2) {
        select.trigger('change.select2')
      }
    }
  
  }
  
  function RgTextArea (_data_id, _options) {
  
    var _this        = this
    var _parent      = null
    var _label       = _options['label']
    var _placeholder = _options['placeholder']
    var _required    = _options['required']
    var _maxlength   = _options['maxlength']
  
    this.getOutput = function () {
      var _required_attrs = ''
      if (_required) {
        _required_attrs = ' aria-required="' + _required + '" required="' + _required + '"'
      }
  
      var _required_markup = ''
      if (_options['required']) {
        _required_markup = '<span class="fb-required">*</span></label>'
      }
  
      var _required_label = ''
      if (_options['display_label_required']) {
        if (_options['required']) {
          _required_label = '<label class="fb-text-label desc">[' + _options['label_required'] + ']</label>'
        }
      }
  
      return '' +
        '<div class="fb-text form-group field-' + _data_id + '">' +
        '<div class="input-group input-formulario" id="input_' + _data_id + '">' +
        '   <label for="' + _data_id + '" class="fb-text-label">' + _label + _required_markup + '</label>' +
        '   <textarea type="text" spellcheck="true" placeholder="' + _placeholder + '" class="form-control input-formulario" name="' + _data_id + '" maxlength="' + _maxlength + '" id="' + _data_id + '" title="' + _placeholder + '"' + _required_attrs + '></textarea>' +
        '' + _required_label +
        '</div>' +
        '</div>'
    }
  
    this.render = function (_parent_selector) {
      this._parent = $(_parent_selector)
      this._parent.append(_this.getOutput())
    }
  
    this.enable = function () {
      this._parent.find('textarea').attr('disabled', false)
    }
  
    this.disable = function () {
      this._parent.find('textarea').attr('disabled', 'disabled')
    }
  
  }
  
  function RgInputText (_data_id, _options) {
  
    var _this        = this
    var _label       = _options['label']
    var _placeholder = _options['placeholder']
    var _required    = _options['required']
    var _maxlength   = _options['maxlength']
  
    this.getOutput = function () {
      var _required_attrs = ''
      if (_required) {
        _required_attrs = ' aria-required="' + _required + '" required="' + _required + '"'
      }
  
      var _required_markup = ''
      if (_options['required']) {
        _required_markup = '<span class="fb-required">*</span></label>'
      }
  
      var _required_label = ''
      if (_options['display_label_required']) {
        if (_options['required']) {
          _required_label = '<label class="fb-text-label desc">[' + _options['label_required'] + ']</label>'
        }
      }
  
      return '' +
        '<div class="fb-text form-group field-' + _data_id + '">' +
        '<div class="input-group input-formulario" id="input_' + _data_id + '">' +
        '   <label for="' + _data_id + '" class="fb-text-label">' + _label + _required_markup + '</label>' +
        '   <input type="text" spellcheck="true" placeholder="' + _placeholder + '" class="form-control input-formulario" name="' + _data_id + '" maxlength="' + _maxlength + '" id="' + _data_id + '" title="' + _placeholder + '"' + _required_attrs + '>' +
        '' + _required_label +
        '</div>' +
        '</div>'
    }
  
    this.render = function (_parent_selector) {
      $(_parent_selector).append(_this.getOutput())
    }
  }
  
  function RgInputFileImage (_data_id, _options, _format_options) {
  
    var _this        = this
    var _parent      = null
    this._format_options  = _format_options
    var _label       = _options['label']
    var _placeholder = _options['placeholder']
    var _required    = _options['required']
    var _maxlength   = _options['maxlength']
  
  
    this.setFormatOptions = function (_format_options) {
      if (this._format_options) {
  
        this.format_options = JSON.parse(JSON.stringify(_format_options))
  
        if (this.format_options.multiple) {
          this._multiple = 'multiple'
        }
  
      }else {
        this._multiple = ''
      }
    }
  
    this.getOutput = function () {
      var _required_attrs = ''
      if (_required) {
        _required_attrs = ' aria-required="' + _required + '" required="' + _required + '"'
      }
  
      var _required_label = ''
      if (_options['display_label_required']) {
        if (_options['required']) {
          _required_label = '<label class="fb-image-label desc">[' + _options['label_required'] + ']</label>'
        }
      }
  
      return (
        "" +
        '<div class="fb-image form-group field-' +
        _data_id +
        '">' +
        '<div class="input-group input-formulario" id="input_' +
        _data_id +
        '">' +
        this.label(_options) +
        '   <input type="file" accept="image/*"' +
        _this._multiple +
        ' placeholder="' +
        _placeholder +
        '" class="form-control input-formulario" name="' +
        _data_id +
        '" maxlength="' +
        _maxlength +
        '" id="' +
        _data_id +
        '" title="' +
        _placeholder +
        '"' +
        _required_attrs +
        " capture>" +
        "" +
        _required_label +
        "</div>" +
        "</div>"
      );
    }
  
  
    this.label = function (_options) {
  
      var _label_html = ''
  
      var _required_markup = ''
      if (_options['required']) {
        _required_markup = '<span class="fb-required">*</span></label>'
      }
  
      if (_options['label']) {
        _label_html = '<label for="' + _data_id + '" class="fb-text-label">' + _options['label'] + _required_markup + '</label>'
      }
  
      return _label_html
  
    }
  
    this.render = function (_parent_selector) {
      this._parent = $(_parent_selector)
      this.setFormatOptions(this._format_options)
      this._parent.append(_this.getOutput())
    }
  
    this.enable = function () {
      this._parent.find('div.imageFile').remove()
      this._parent.find('input[type="file"]').attr('disabled', false)
    }
  
    this.disable = function () {
      this._parent.find('input[type="file"]').attr('disabled', 'disabled')
    }
  
  }
  
  function RgInputHidden (_name, _default_value) {
  
    var _this          = this
    this.name          = _name
    this.default_value = _default_value
  
    this.getOutput = function () {
      return '<input type="hidden" name="' + _this.name + '" id="' + _this.name + '" value/>'
    }
  
    this.render = function (_parent_selector) {
      $(_parent_selector).append(_this.getOutput())
    }
  
  }
  
  function RgInputNumeric (_data_id, _format_options) {
  
    var _this          = this
    this._data_id      = _data_id
    this._defaultValue = 0
    this._min          = 0
    this._max          = 100
    this._step         = 1
    this._decimal      = false
    this._disabled     = false
    this._spinner      = null
    this._dataDepends  = false
  
    this.wrapper_html = '<fieldset>' +
      '<div id="input-{{dataId}}" class="input-wrap input-numeric" data-id="{{dataId}}">' +
      '{{body}}' +
      '</div>' +
      '</fieldset>'
  
    this.input_html = '<input type="number" min="0" class="spinner-{{dataId}}" name="{{dataId}}" value=>'
  
    this.getOutput = function () {
      var _output_html          = _this.wrapper_html.split('{{dataId}}').join(this._data_id)
      var _output_input_element = _this.input_html.split('{{dataId}}').join(this._data_id)
      return _output_html.replace('{{body}}', _output_input_element)
    }
  
    this.render = function (_parent_selector) {
      $(_parent_selector).append(_this.getOutput())
      _this.init()
    }
  
    this.init = function () {
  
      if (typeof _format_options !== 'undefined') {
  
        if (typeof _format_options['defaultValue'] !== 'undefined') {
          this._defaultValue = _format_options['defaultValue']
        }
  
        if (typeof _format_options['min'] !== 'undefined') {
          this._min = _format_options['min']
        }
  
        if (typeof _format_options['max'] !== 'undefined') {
          this._max = _format_options['max']
        }
  
        if (typeof _format_options['decimal'] !== 'undefined') {
          this._decimal = _format_options['decimal']
        }
  
        if (typeof _format_options['step'] !== 'undefined') {
          this._step = _format_options['step']
        }
  
        if (typeof _format_options['disabled'] !== 'undefined') {
          this._disabled = _format_options['disabled']
        }
  
        if (typeof _format_options['dataDepends'] !== 'undefined') {
          this._dataDepends = _format_options['dataDepends']
        }
  
      }
  
      let spinner_options = {}
      spinner_options.max =  this._max ? this._max : null
      spinner_options.min =  this._min ? this._min : 0
      spinner_options.culture =  "es-ES"
      spinner_options.numberFormat =  "C"
      spinner_options.step =  this._step ? this._step : this._decimal ? 0.01 : 1
      // spinner_options.numberFormat =  options.decimal ? "C" : null
  
      var spinner = $('.spinner-' + this._data_id)
      spinner.spinner(spinner_options)
      spinner.spinner('value', this._defaultValue)
  
      _this._spinner = spinner
  
      if (this._disabled) {
          this.disable()
      }
  
    }
  
    this.enable = function () {
      this._spinner.spinner('enable')
    }
  
    this.disable = function () {
      this._spinner.spinner('disable')
    }
  
  }
  
  function RgSignature(_data_id, _format_options) {
  
    var _this          = this
    this._data_id      = _data_id
    this._signature_pad = null
  
  
    this.wrapper_html = '<div id="wrapper-{{dataId}}" class="wrapper-signature" data-id="{{dataId}}">' +
        '{{body}}' +
        '</div><div><button id="clear-signature">Clear</button></div>'
  
    this.input_html = '<canvas id="signature-pad-{{dataId}}" class="signature-pad" width=400 height=200></canvas>'
  
    this.getOutput = function () {
      var _output_html          = _this.wrapper_html.split('{{dataId}}').join(this._data_id)
      var _output_input_element = _this.input_html.split('{{dataId}}').join(this._data_id)
      return _output_html.replace('{{body}}', _output_input_element)
    }
  
    this.render = function (_parent_selector) {
      $(_parent_selector).append(_this.getOutput())
      _this.init()
    }
  
    this.init = function () {
      this._signature_pad = new SignaturePad(document.getElementById('signature-pad-'+this._data_id), {
        backgroundColor: 'rgba(255,255,255,0)',
        penColor: 'rgb(0, 0, 0)'
      });
  
    }
  
    this.clean = function () {
      this._signature_pad.clear();
    }
  
    this.get_signature_pad = function (){
      return this._signature_pad
    }
  
    this.enable = function () {
    }
  
    this.disable = function () {
    }
  
  }
  
  function JSONtoOptions (_json, _json_value_field, _json_label_field, _json_label_img = null) {
  
    var _options = {}
    var _counter = 1
    _json.forEach(function (jsonData) {
      var _element       = {}
      _element.value     = jsonData[_json_value_field]
      _element.label     = jsonData[_json_label_field]
      if(_json_label_img) {
        _element.title = jsonData[_json_label_img]
      }
      _options[_counter] = _element
      _counter           = _counter + 1
  
    })
    return _options
  
  }
  
  function JSONtoOptionsLabelFields (_json, _json_value_field, _json_label_fields, label_separator, _json_label_img = null) {
  
    if (!label_separator) {
      label_separator = ','
    }
  
    var _options = {}
    var _counter = 1
    _json.forEach(function (jsonData) {
      var _element   = {}
      _element.value = jsonData[_json_value_field]
      _element.label = ''
      _json_label_fields.forEach(function (_json_label_field) {
        if (_json_label_field && _json_label_field !== '' && jsonData[_json_label_field]) {
          _element.label += jsonData[_json_label_field] + label_separator
        }
      })
  
      if(_json_label_img) {
        _element.title = jsonData[_json_label_img]
      }
  
      _element.label = _element.label.split(',,').join('label_separator')
      _element.label = _element.label.split(', ,').join('label_separator')
      _element.label = _element.label.split(',  ,').join('label_separator')
      _element.label = _element.label.slice(0, -1)
  
      _options[_counter] = _element
      _counter           = _counter + 1
  
    })
    return _options
  
  }
  
  function JSONFilterToOptions (_json, _json_value_field, _json_label_field) {
  
    var _options = {}
    var _counter = 1
    $.each(_json, function (i, item) {
      var _element       = {}
      _element.value     = item[_json_value_field]
      _element.label     = item[_json_label_field]
      _options[_counter] = _element
      _counter           = _counter + 1
  
    })
  
    return _options
  }
  
  function JSONtoOptionsTest (_json, _json_value_field, _json_label_field) {
  
    var _options     = {}
    var _value_field = _json_value_field
    var _label_field = _json_label_field
  
    var _counter = 1
  
    $.each(_json, function (i, item) {
      var _element       = {}
      _element.value     = item[_value_field]
      _element.label     = getLabelOptions(item, _label_field)
      _options[_counter] = _element
      _counter           = _counter + 1
  
    })
  
    return _options
  }
  
  function getLabelOptions (item, _label_field) {
  
    if (!_label_field.template || !_label_field.fields) {
      console.log('11111111111111')
      return item[_label_field]
    }
  
    var _key, _field_name
    var _result = _label_field.template
    _label_field.fields.forEach(function (element, index, array) {
      _key        = Object.keys(element)[0]
      _field_name = element[_key]
      _result     = _result.replace('{{' + _key + '}}', item[element[_key]])
    })
  
    return _result
  }
  