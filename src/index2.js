export * as index2 from './index2.js';

var _tabs_x_modalidad = {
    '1': { '0': 'laboratorio_tab1', '1': 'laboratorio_tab2' },
  }
  
  var _question_response_formats = {
    'si_no':                    { 'class': '_rg_response_format_yes_no' },
    '1':                        { 'class': '_rg_response_format_1_options' },
    '2':                        { 'class': '_rg_response_format_2_options' },
    '3 opciones':               { 'class': '_rg_response_format_3_options' },
    '3':                        { 'class': '_rg_response_format_3' },
    '4':                        { 'class': '_rg_response_format_4' },
    '5':                        { 'class': '_rg_response_format_5' },
    '6':                        { 'class': '_rg_format_bueno_malo_response_labo' },
    'caritas':                  { 'class': '_rg_format_bueno_malo_response_labo' },
    'anchoas':                  { 'class': '_rg_format_bueno_malo_response_labo w120px' },
    '9 opciones':               { 'class': '_rg_response_format_10_options' },
    '10 opciones':              { 'class': '_rg_response_format_10_options' },
    '11 opciones':              { 'class': '_rg_response_format_10_options' },
    'raviolis':                 { 'class': '_rg_format_bueno_malo_response_labo' },
    'checkboxgroup':            { 'class': '_rg_format_checkbox' },
    'checkboxgroupthisandthat': { 'class': '_rg_format_checkboxthisandthat' },
    'radiogroup':               { 'class': '_rg_format_radiogroup' },
    'radiogroupthisandthat':    { 'class': '_rg_format_radiothisandthat' },
    'select':                   { 'class': '_rg_format_select' },
    'inputText':                { 'class': '_rg_format_text' },
    'textarea':                 { 'class': '_rg_format_textarea' },
    'fileImage':                { 'class': '_rg_format_image' },
    'numeric':                  { 'class': '_rg_format_numeric' },
    'date':                     { 'class': '_rg_format_date' },
    'rtl':                      { 'class': '_rg_rtl' },
    'options':                  { 'class': '_rg_format_options' },
    'signature':                  { 'class': '_rg_format_signature' }
  }
  
  var _panel_styles = '' +
    '   background-image: url(/img/fondo_paneles_default.png));' +
    '   background-color: rgba(255,255,255,1.00);' +
    '   border-radius: 4px;' +
    '   box-shadow: 0px 0px 5px 1px rgba(0,0,0,0.15);' +
    ''
  
  function Behaviour (_survey) {
  
    var _this   = this
    this.survey = ''
  
    if (typeof _survey !== 'undefined') {
      this.survey = _survey
    }
  
    this.behaviourPasarPantalla = function () {
      var _actual_tab = $(this).closest('.tab-pane.fade.in.active')
      _actual_tab.removeClass('in active')
  
      var _actual_tab_id = parseInt(_actual_tab.attr('id'))
      $('#' + (_actual_tab_id + 1) + '.tab-pane.fade').addClass('in active')
    }
  
    this.behaviourNextTab = function () {
      var _actual_tab = $(this).closest('.tab-pane.fade.in.active')
      _actual_tab.removeClass('in active')
  
      var _actual_tab_id = parseInt(_actual_tab.attr('id'))
      $('#' + (_actual_tab_id + 1) + '.tab-pane.fade').addClass('in active')
    }
  
    this.behaviourRetrocederPantalla = function () {
      var _actual_tab = $(this).closest('.tab-pane.fade.in.active')
      _actual_tab.removeClass('in active')
  
      var _actual_tab_id = parseInt(_actual_tab.attr('id'))
      $('#' + (_actual_tab_id - 1) + '.tab-pane.fade').addClass('in active')
    }
  
    this.volverAInicio = function () {
  
      var _actual_tab = $(this).closest('.tab-pane.fade.in.active')
      _actual_tab.removeClass('in active')
  
      $('input.input-formulario').val('')
      $('select option:selected').attr('selected', false)
      $('select option[value="-1"]').attr('selected', true)
      $('input[type="checkbox"]').attr('checked', false)
      $('.chosen-select-deselect').trigger('chosen:updated')
  
      $('form#fpage2 input[name="leadid"]').val(0)
  
      var _tooltipsters = $('.tooltipstered')
      _tooltipsters.tooltipster('close')
      _tooltipsters.css('border-color', '#ccc')
      _tooltipsters.prev().css('border-color', '#ccc')
  
      $('#0.tab-pane.fade').addClass('in active')
    }
  
    this.reloadPage = function () {
      if (navigator.onLine) {
        if (_this.survey !== null && _this.survey.offline !== null) {
          if (typeof _this.survey.offline != 'undefined' && _this.survey.offline.isOffline) {
            _this.survey.offline.pushQueue()
            _this.survey.offline.unregisterSw()
          }
        }
        window.location.reload(true)
      } else {
        _this.survey.reset()
      }
    }
  
  }
  
  function SurveyTab () {
  
    var _this             = this
    this.original_id      = -1
    this.name             = ''
    this.element          = null
    this.panel            = null
    this._objectname      = 'SurveyTab'
    this._hidden          = false
    this.background_image = null
    this.logo_image       = null
  
    this.actionSelector = null
    this.actionEvent    = null
    this.actionHandler  = null
  
    this.heightStyles = true
  
    this.getOriginalId = function () {
      return this.original_id
    }
  
    this.getName = function () {
      return this.name
    }
  
    this.getElement = function () {
      return this.element
    }
  
    this.setElement = function (_element) {
      this.element = _element
      this.panel   = this.element.find('.panel')
    }
  
    this.getPanel = function () {
      return this.panel
    }
  
    this.render = function (index) {
      this.element.attr('id', index)
      this.element.addClass(this.name)
    }
  
    this.addTitle = function (_title) {
      if (_title != null && _title != '') {
        var _title_html = '<h4 class="titulo-encuesta">' + _title + '</h4>'
        $(_title_html).prependTo(this.panel)
      }
    }
  
    this.addSubtitle = function (_subtitle) {
      if (_subtitle != null && _subtitle != '') {
        var _title_html = '<h2 class="tab_panel_subtitle">' + _subtitle + '</h2>'
        $(_title_html).insertAfter(this.panel.find('h3.pregunta'))
      }
    }
  
    this.addText = function (_text) {
      if (_text != null && _text != '') {
        var _text_html = '<p class="texto-encuesta">' + _text + '</p>'
        $(_text_html).prependTo(this.panel)
      }
    }
  
    this.addImage = function (_src, _alt) {
      var _image_html = '<div class="panel_img"><img class="tab_panel_image" src="' + _src + '" border="0"/></div>'
      $(_image_html).prependTo(this.panel)
    }
  
    this.addImageBeforeButton = function (_src, _alt) {
      var _image_html = '<div class="panel_img"><img class="tab_panel_image" src="' + _src + '" border="0"/></div>'
      this.panel.find('button').first().before(_image_html)
    }
  
    this.addVideoBeforeButton = function (_src, _poster) {
      var _video_html = '<div class="panel_video"><video poster="' + _poster + '" playsinline autoplay controls class="tab_panel_video" id="myvideo" src="' + _src + '" width="300px" height="300px" ></video></div>'
      this.panel.find('button').first().before(_video_html)
    }
  
    this.addForm = function (_id, _name, _action, _class, _container_class, _selector) {
      _form_html = '<div class="' + _container_class + '">' +
        '<form action="' + _action + '" id="' + _id + '" name="' + _name + '" class="' + _class + '"></form>' +
        '</div>'
      if (typeof _selector != 'undefined') {
        $(_form_html).appendTo(_selector)
      } else {
        $(_form_html).appendTo(this.panel)
      }
    }
  
    this.addButton = function (_id, _type, _class, _style, _text, _form_id, _active, _content, _selector) {
  
      var _new_button = new Button(_id, _type, _class, _style, _text, null, _content)
      _new_button.setSurveyTab(this)
  
      if (_selector) {
        $(_new_button.render()).appendTo(this.getPanel().find(_selector))
      } else if (_form_id) {
        $(_new_button.render()).appendTo($('form#' + _form_id + ' fieldset'))
      } else {
        $(_new_button.render()).appendTo(this.getPanel())
      }
  
      if (_active) {
        //_my_log('SurveyTab.addButton: Active Button!');
      }
    }
  
    this.test = function () {
      console.log('echo test')
    }
  
    this.getObjectName = function () {
      return this._objectname
    }
  
    this.hiddenTab = function () {
      this._hidden = true
    }
  
    this.displayTab = function () {
      this._hidden = false
    }
  
    this.isVisible = function () {
      return !this._hidden
    }
  
    this.beforeActivate = function () {
      return true
    }
  
    this.afterActivate = function () {
      return true
    }
  
    this.beforeDeactivate = function () {
      return true
    }
  
    this.afterDeactivate = function () {
      return true
    }
  
    this.translate = function (lang_code) {
      //_my_log('TRANSLATE | SurveyTab.translate | Tab name:'+_this.name+' | lang_code:'+lang_code);
    }
  
    this.activate = function () {
  
      var _me = this
  
      if (this.isVisible()) {
  
        if (this.getBackgroundImage()) {
          $('div#main').css('background-image', 'url("' + this.getBackgroundImage() + '")')
        }
  
        if (this.getLogoImage()) {
          $('#logo-navbar').attr('src', this.getLogoImage())
        }
  
        //if(this.name) {
        $('body').addClass('tab_' + this.name)
        //}
  
        if (this.beforeActivate()) {
          this.getElement().addClass('in')
          this.getElement().addClass('active')
          window.scrollTo(0, 1)
          /*if (this.isEnableHeightStyles()) {
           if (this.isEnableHeightStyles()) {
           $(this.getPanel().find('div:last-child')).ready(function () {
           _me.setHeightStyles();
           });                    }
           }*/
          return this.afterActivate()
        }
      }
      return false
    }
  
    this.deactivate = function () {
      if (this.beforeDeactivate()) {
        $('body').removeClass('tab_' + this.name)
        this.getElement().removeClass('in')
        this.getElement().removeClass('active')
        return this.afterDeactivate()
      }
      return false
    }
  
    //Deprecated. Usar mejor setTabAction
    this.setAction = function (_actionSelector, _actionEvent, _actionHandler, _off) {
  
      this.actionSelector = _actionSelector
      this.actionEvent    = _actionEvent
      this.actionHandler  = _actionHandler
  
      //apagamos los eventos iguales
      if (_off) {
        $(_actionSelector).off(_actionEvent)
        $(document).off(_actionEvent, _actionSelector)
      } else {
      }
  
      //activamos evento
      $(_actionSelector).on(_actionEvent, _actionHandler)
    }
  
    this.setTabAction = function (_actionSelector, _actionEvent, _actionHandler, _off) {
  
      this.actionSelector = _actionSelector
      this.actionEvent    = _actionEvent
      this.actionHandler  = _actionHandler
  
      //apagamos los eventos iguales
      if (_off) {
        $(_actionSelector).off(_actionEvent)
        $(document).off(_actionEvent, _actionSelector)
      } else {
      }
  
      //activamos evento
      $('.' + this.getName() + ' ' + _actionSelector).on(_actionEvent, _actionHandler)
    }
  
    this.setSize = function (_old_size, _new_size) {
      var _div_element = this.getPanel().parent()
      _div_element.removeClass(_old_size)
      _div_element.addClass(_new_size)
    }
  
    this.setBackgroundImage = function (_image_src) {
      _this.background_image = _image_src
    }
  
    this.getBackgroundImage = function () {
      return _this.background_image
    }
  
    this.setLogoImage = function (_image_src) {
      _this.logo_image = _image_src
    }
  
    this.getLogoImage = function () {
      return _this.logo_image
    }
  
    this.getTabType = function () {
      return 'SurveyTab'
    }
  
    this.defaultAction = function () {
  
    }
  
    this.setTimeout = function (timeout) {
      //_my_log('| SurveyTab.setTimeout');
      if (timeout) {
        //_my_log('| SurveyTab.setTimeout.timeout:'+timeout);
        this._timeout = timeout
      }
    }
  
    this.disableTimeout = function () {
      //_my_log('| SurveyTab.disableTimeout');
      this._timeout = 0
    }
  
    this.setHeader = function (_header_text, _showLegend) {
  
      if (_header_text != null && _header_text != '') {
        var _legend = ''
        if (_showLegend) {
          _legend = '<a class="open_legend"><i class="fa fa-question-circle"></i></a>'
        }
        var _header_block_html = '' +
          '<div class="header-block">' + _header_text + _legend + '</div>'
        $(_header_block_html).insertBefore(this.getPanel())
      }
    }
  }
  
  function LaboratorioSurveyTab () {
    this._objectname = 'LaboratoioSurveyTab'
  }
  
  LaboratorioSurveyTab.prototype             = new SurveyTab()
  LaboratorioSurveyTab.prototype.constructor = SurveyTab
  
  function LaboratorioSurveyTab1 () {
  
    var _this        = this
    this._objectname = 'LaboratorioSurveyTab1'
    this.onlyOne     = false
    this._timeout    = 0
  
    this._tabQuestions = []
    this.setQuestions  = function (questions) {
      questions.forEach(function (questionObject) {
        _this._tabQuestions[questionObject.getId()] = questionObject
      })
  
    }
  
    this.beforeActivate = function () {
      if (this._timeout > 0) {
        timeout = setTimeout('location.reload(true);', this._timeout)
      }
      return true
    }
  
    this.clean = function () {
  
      var question_modal = _this.getPanel().closest('#main').find('.modal').find('.grupo-pregunta')
      question_modal.each(function (index, element) {
        var _id = $(element).attr('data-id')
        new Question(_id).clean()
      })
  
      var question = this.getPreguntas()
      question.each(function (index, element) {
        var _id = $(element).attr('data-id')
        new Question(_id).clean()
      })
    }
  
    this.getPreguntas = function () {
      return _this.getPanel().find('.grupo-pregunta')
    }
  
    this.moveForm = function (_tab_target, _before_to) {
  
      var _form_title  = this.getPanel().find('h4.titulo-encuesta')
      var _form_text   = this.getPanel().find('p.texto-encuesta')
      var _form        = this.getPanel().find('form.form-encuesta')
      var _form_button = this.getPanel().find('#btn1-laboratorio-encuesta')
  
      var _target_panel = _tab_target.getPanel()
      var _target_element
      if (_before_to) {
        _target_element = _tab_target.getPanel().find(_before_to)
      } else {
        _target_element = _target_panel
      }
  
      _target_element.append(_form_title)
      _target_element.append(_form_text)
      _target_element.append(_form)
      _target_element.append(_form_button)
      //Utilizan 'panel-derecha' para guardar los datos del formulario ver wizard.js volverInicio
      _target_panel.addClass('panel-derecha')
  
      //Borramos el panel donde se muestra por defecto el formulario
      this.getElement().find('.panel-derecha').parent().remove()
  
      //Actualizamos estilos del panel izquierda para mostrarlo centrado
      this.getElement().find('.panel-izquierda').addClass('panel-central')
      this.getElement().find('.panel-izquierda').parent().addClass('centrado')
      this.getElement().find('.panel-izquierda').removeClass('panel-izquierda')
  
    }
  
    this.moveButton = function (_tab_target, _button_selector, _selector_target) {
  
      if (typeof _button_selector === 'undefined' || _button_selector == null) {
        _button_selector = '#btn1-laboratorio'
      }
  
      var _button = this.getPanel().find(_button_selector)
  
      if (_selector_target) {
        $(_selector_target).append(_button)
      } else {
        _tab_target.getPanel().append(_button)
      }
    }
  
    this.mergePanels = function () {
      var _right_panel = this.getRightPanel()
      var _left_panel  = this.getLeftPanel()
      _right_panel.children().appendTo(_left_panel)
      _right_panel.parent().remove()
      _left_panel.removeClass('panel-izquierda')
      _left_panel.addClass('panel-central')
      _left_panel.parent().addClass('centrado')
      _this.panel = _left_panel
    }
  
    this.getRightPanel = function () {
      return this.getElement().find('.panel-derecha')
    }
  
    this.getLeftPanel = function () {
      return this.getElement().find('.panel-izquierda')
    }
  
    this.setFormTitle = function (_text) {
      this.getElement().find('h4.titulo-encuesta').html(_text)
    }
  
    this.removeFormTitle = function () {
      this.getElement().find('h4.titulo-encuesta').remove()
    }
  
    this.setFormText = function (_text) {
      this.getElement().find('.texto-encuesta').html(_text)
    }
  
    this.removeFormText = function () {
      this.getElement().find('.texto-encuesta').remove()
    }
  
    this.translate = function (lang_code) {
      //_my_log('TRANSLATE | LaboratorioSurveyTab1.translate | Tab name:'+_this.name+' | lang_code:'+lang_code);
  
      //Header-Block
      var headerBlock = _this.getPanel().siblings('.header-block')
      if (headerBlock) {
        headerBlock.each(function () {
          $(this).html(translate($(this).html().trim(), lang_code))
        })
      }
  
      //Legends
      var legend = _this.getPanel().find('.legend').children()
      if (legend) {
        legend.each(function () {
          $(this).html(translate($(this).html().trim(), lang_code))
        })
      }
  
      //titulo encuesta
      var titulo_encuesta = _this.getPanel().find('.titulo-encuesta')
      if (titulo_encuesta) {
        titulo_encuesta.each(function () {
          $(this).html(translate($(this).html().trim(), lang_code))
        })
      }
  
      //titulo encuesta
      var pregunta_title = _this.getPanel().find('.pregunta-title')
      if (pregunta_title) {
        pregunta_title.each(function () {
          $(this).html(translate($(this).html().trim(), lang_code))
        })
      }
  
      //Translate Questions
      var allTabQuestions = _this.getPanel().find('.grupo-pregunta')
      allTabQuestions.each(function (index, _question_element) {
        var _questionId = _question_element.getAttribute('data-id')
        if (_questionId !== 'undefined') {
          var _question = new Question(_questionId)
          _question.translate(lang_code)
        } else {
          _allQuestionText = _this.getPanel().find('.pregunta')
          _allQuestionText.each(function () {
            $(this).html(translate($(this).html().trim(), lang_code))
          })
        }
      })
  
      _this.translateButtons(lang_code)
  
      //Translate Titles
      _allTabTitles = _this.getPanel().find('.titulo-encuesta')
      _allTabTitles.each(function () {
        $(this).html(translate($(this).html().trim(), lang_code))
      })
  
      //Translate Placeholders
      _allTabPlaceholders = _this.getPanel().find('input[type=textarea], input[type=password], textarea, input[type=text]')
      _allTabPlaceholders.each(function () {
        $(this).attr('placeholder', translate($(this).attr('placeholder'), lang_code))
      })
  
      //Texts
      var textToTranslate = _this.getPanel().find('.text-to-translate')
      if (textToTranslate) {
        textToTranslate.each(function (index) {
          $(this).html(translate($(this).html().trim(), lang_code))
        })
      }
  
      //Tooltipter
      var _allTooltipster = $('body').find('.tooltipster-content');
      _allTooltipster.each(function () {
        $(this).html(translate($(this).html().trim(), lang_code))
      })
  
    }
  
    this.translateButtons = function (lang_code) {
      var allTabButtonsPanel = _this.getPanel().find('button')
  
      allTabButtonsPanel.each(function () {
        buttonTranslate(this, lang_code)
      })
    }
  
    this.clone = function (_name) {
      var _clon         = new LaboratorioSurveyTab1()
      _clon.name        = _name
      _clon._objectname = _this.getObjectName()
      _clon.element     = _this.getElement().clone()
      _clon.element.removeClass(_this.name)
      _clon.element.addClass(_clon.name)
      _clon.panel          = _clon.element.find('.panel')
      _clon.actionSelector = _this.actionSelector
      _clon.actionEvent    = _this.actionEvent
      _clon.actionHandler  = _this.actionHandler
      if (this.isEnabledOnlyOneAnswer()) {
        _clon.enableOnlyOneAnswer()
      }
      return _clon
    }
  
    this.questionByTabLab = function (idQuestions, _questions) {
  
      if (typeof (idQuestions) === 'undefined') {
        idQuestions = [0]
      }
  
      var _idQuestionsLength = idQuestions.length
      var _questionsFound    = 0
  
      if (typeof _questions !== 'undefined' && _questions != null) {
  
        console.log('questionByTabLab new')
        _questions.each(function (index) {
          var _question_id = _questions[index].getAttribute('data-id')
          if (_question_id !== 'undefined') {
            if (idQuestions.indexOf(parseInt(_question_id)) !== -1) {
              _this.getPanel().append(_questions[index])
              _questionsFound++
            }
          }
          if (_questionsFound === _idQuestionsLength) {
            return false
          }
        })
  
      } else {
  
        var questLab = _this.getPanel().find('.grupo-pregunta')
        questLab.each(function (index) {
          var _quest = questLab[index].getAttribute('data-id')
          if (_quest !== 'undefined') {
            if (idQuestions.indexOf(parseInt(_quest)) === -1) {
              _this.removeQuestion(_quest)
              _questionsFound++
            }
          }
        })
  
      }
  
      return _questions
    }
  
    this.questionsOrderedByTabLab = function (idQuestions, _questions) {
  
      if (typeof (idQuestions) === 'undefined') {
        idQuestions = [0]
      }
  
      var _idQuestionsLength = idQuestions.length
      var _questionsFound    = 0
  
      if (typeof _questions !== 'undefined' && _questions != null) {
  
        idQuestions.forEach(function (idQuestion) {
          _questions.each(function (index) {
            var _question_id = _questions[index].getAttribute('data-id')
            if (_question_id !== 'undefined') {
              if (idQuestion == _question_id) {
                _this.getPanel().append(_questions[index])
                _questionsFound++
              }
            }
          })
          if (_questionsFound === _idQuestionsLength) {
            return false
          }
        })
  
      }
  
      return _questions
    }
  
    this.onlyOneAnswer = function (Quest) {
      var grupo_pregunta = $(Quest).closest('.panel').find('.grupo-pregunta')
      grupo_pregunta.each(function (index) {
        var _input = $(this).find('input[name=\'rating_rateandgrade\']')
  
        if (Quest.id !== _input.attr('id')) {
          // Get IE or Edge browser version
          var version = detectIE()
          if (version === false) {
            $(this).find('span.star:first-child').css('content', 'url(/subidas-cliente/repsol1516640980/recursos/logo_navbar/select_OFF.png')
          } else {
            $(this).find('img').attr('src', '/subidas-cliente/repsol1516640980/recursos/logo_navbar/select_OFF.png')
          }
          //Inicializar valor del input[rating_rateandgrade]
          _input.val(0)
        }
  
      })
    }
  
    this.disableOnlyOneAnswer   = function () {
      if (this.onlyOne) {
        _this.getPanel().find('input[name=\'rating_rateandgrade\']').off('rating.change')
        this.onlyOne = false
      }
    }
    this.isEnabledOnlyOneAnswer = function () {
      return this.onlyOne
    }
  
    this.enableOnlyOneAnswer = function () {
      if (!this.isEnabledOnlyOneAnswer()) {
        _this.getPanel().find('input[name=\'rating_rateandgrade\']').on('rating.change', function () {
          _this.onlyOneAnswer(this)
        })
        this.onlyOne = true
      }
    }
  
    this.removeQuestion = function (_question_id) {
      _this.getPanel().find('.grupo-pregunta[data-id="' + _question_id + '"]').remove()
    }
  
    this.removeInputForm = function (_input_id) {
      _this.getPanel().find('#' + _input_id + '.input-group.input-formulario').remove()
    }
  
    this.hiddenInputForm = function (_input_id) {
      _this.getPanel().find('#' + _input_id + '.input-group.input-formulario').hide()
    }
  
    this.hiddenButton = function (_button_id) {
      _this.getPanel().find('#' + _button_id).hide()
    }
  
    this.hiddenCheckbox = function (_checkbox_id) {
      _this.getPanel().find('#' + _checkbox_id).parent().hide()
    }
  
    this.changeButtonId = function (_new_name) {
      var _button = _this.getPanel().find('button').last()
      _button.attr('id', _new_name)
    }
  
    this.removeClassButton = function (_class_name) {
      var _button = this.getPanel().find('button')
      _button.removeClass(_class_name)
    }
  
    this.removeForm = function () {
      _this.getPanel().find('form.form-encuesta').remove()
    }
  
    this.removePanelForm = function () {
      _this.getElement().find('.panel.panel-derecha').remove()
    }
  
    this.removeButton = function (_selector) {
      if (_selector) {
        this.getPanel().find(_selector).remove()
      } else {
        this.getPanel().find('button').remove()
      }
    }
  
    this.hiddenButton = function () {
      var _button = this.getPanel().find('button')
      _button.addClass('hidden')
    }
  
    this.addFormTitle = function (title) {
      _this.getPanel().prepend('<h4 class="titulo-encuesta">' + title + '</h4>')
    }
  
    this.addGrupoPregunta = function (pregunta, content, data_id, _class) {
  
      var _titlePregunta = ''
  
      if (typeof pregunta !== 'undefined' && pregunta !== null) {
        _titlePregunta = '<h3 class="pregunta" style="color: #000000" data-id="' + data_id + '">' +
          pregunta +
          '</h3>'
      }
  
      var _html = '' +
        '<div class="grupo-pregunta ' + _class + '" data-id="' + data_id + '">' +
        _titlePregunta +
        '' + content +
        '</div>'
      _this.getPanel().prepend(_html)
    }
  
    this.addGrupoPreguntaBeforeButton = function (pregunta, content, data_id, _class) {
  
      var _html = '' +
        '<div class="grupo-pregunta ' + _class + '" data-id="' + data_id + '">' +
        '<h3 class="pregunta" style="color: #000000" data-id="' + data_id + '">' +
        '' + pregunta + '' +
        '</h3>' +
        '' + content +
        '</div>'
      _this.getPanel().find('button').first().before(_html)
    }
  
    this.setTimeout = function (timeout) {
      if (timeout) {
        this._timeout = timeout
      }
    }
  }
  
  LaboratorioSurveyTab1.prototype             = new LaboratorioSurveyTab()
  LaboratorioSurveyTab1.prototype.constructor = LaboratorioSurveyTab
  
  function LaboratorioSurveyTab2 () {
    this._objectname = 'LaboratorioSurveyTab2'
    this._timeout    = 4000
    var _this        = this
  
    this.beforeActivate = function () {
      if (this._timeout > 0) {
        timeout = setTimeout(new Behaviour(_survey).reloadPage, this._timeout)
      }
      return true
    }
  
    this.setTimeout = function (timeout) {
      if (timeout) {
        this._timeout = timeout
      }
    }
  
    this.disableTimeout = function () {
      this._timeout = 0
    }
  
    this.translate = function (lang_code) {
      //_my_log('TRANSLATE | LaboratorioSurveyTab2.translate | Tab name:'+_this.name+' | lang_code:'+lang_code);
  
      var titulo_gracias = _this.getPanel().find('.titulo-gracias')
      if (titulo_gracias) {
        titulo_gracias.each(function () {
          $(this).html(translate($(this).html().trim(), lang_code))
        })
      }
  
      //mesnaje_gracias
      var mensaje_gracias = _this.getPanel().find('.mensaje-gracias')
      if (mensaje_gracias) {
        mensaje_gracias.each(function () {
          $(this).html(translate($(this).html().trim(), lang_code))
        })
      }
  
      //Texts
      var textToTranslate = _this.getPanel().find('.text-to-translate')
      if (textToTranslate) {
        textToTranslate.each(function (index) {
          $(this).html(translate($(this).html().trim(), lang_code))
        })
      }
  
      _this.translateButtons(lang_code)
  
    }
  
    this.translateButtons = function (lang_code) {
      var allTabButtonsPanelGracias = _this.getElement().find('.panel-gracias').find('button')
  
      allTabButtonsPanelGracias.each(function () {
        buttonTranslate(this, lang_code)
      })
  
    }
  
    this.clean = function () {
    }
  
  }
  
  LaboratorioSurveyTab2.prototype             = new LaboratorioSurveyTab()
  LaboratorioSurveyTab2.prototype.constructor = LaboratorioSurveyTab
  
  function Question (_id) {
  
    var _this            = this
    this.id              = _id
    this._responseFormat = null
    this._slug_key       = null
    this._element        = null
  
    this.clean = function () {
  
      var _element = _this.getElement()
  
      //LABO ESTRELLAs - CARITAS
      if (_element.find('.rating-laboratorio').length > 0) {
        _element.find('.rating-laboratorio[data-id=' + _this.id + '] .star').each(function (index, element) {
          element.style.content = element.style.content.replace('ON', 'OFF')
          _element.find('.rating-laboratorio[data-id=' + _this.id + '] input[name=\'rating_rateandgrade\']').rating('clear')
        })
      }
  
      //NEAGE CARITAS ESTRELLAS
      if (_element.find('.rating-newage').length > 0) {
        _element.find('.rating-newage[data-id=' + _this.id + '] .star').each(function (index, element) {
          element.style.content = element.style.content.replace('ON', 'OFF')
          _element.find('.rating-newage[data-id=' + _this.id + '] input[name=\'rating_rateandgrade\']').rating('clear')
        })
      }
  
      //TEXTAREA RESPONSE FORMAT
      if (_element.hasClass('_rg_format_textarea')) {
        _element.find('.input-group.input-formulario#input_' + _this.id + ' textarea').each(function (index, element) {
          $(element).val('')
        })
      }
  
      //NUMERIC RESPONSE FORMAT
      if (_element.hasClass('_rg_format_numeric')) {
        _element.find('input[type=number]').each(function (index, element) {
          $(element).val(0)
          _element.find('.rating-laboratorio[data-id=' + _this.id + '] input[name=\'rating_rateandgrade\']').val('')
        })
      }
  
      //SELECT RESPONSE FORMAT
      if (_element.hasClass('_rg_format_select')) {
        if (_element.find('.chosen-container')) {
          _element.find('select#select-' + _this.id).val('').trigger('chosen:updated')
        }
        if (_element.find('.select2')) {
          _element.find('select#select-' + _this.id).val('').trigger('change.select2')
        }
        _element.find('.rating-laboratorio[data-id=' + _this.id + '] select[name=select-' + _this.id + '] option').each(function (index, element) {
          $(element).prop('selected', false)
          if (_element.find('.rating-laboratorio[data-id=' + _this.id + '] input[name=\'rating_rateandgrade\']')) {
            _element.find('.rating-laboratorio[data-id=' + _this.id + '] input[name=\'rating_rateandgrade\']').val('')
          }
          if (_element.find('.rating-laboratorio[data-id=' + _this.id + '] input[name=\'rating_rateandgrade[]\']')) {
            _element.find('.rating-laboratorio[data-id=' + _this.id + '] input[name=\'rating_rateandgrade[]\']').val('')
          }
        })
      }
  
      //RADIOBUTTON
      if (_element.hasClass('_rg_format_radiogroup')) {
        _element.find('.rating-laboratorio[data-id=' + _this.id + '] div#radiogroup-' + _this.id + ' input').each(function (index, element) {
          $(element).prop('checked', false).checkboxradio('refresh')
          $(element).find('.rating-laboratorio[data-id=' + _this.id + '] input[name=\'rating_rateandgrade\']').val('')
        })
      }
  
      //CHECKBOX
      if (_element.hasClass('_rg_format_checkbox')) {
        _element.find('.rating-laboratorio[data-id=' + _this.id + '] div#checkboxgroup-' + _this.id + ' input').each(function (index, element) {
          $(element).prop('checked', false).checkboxradio('refresh')
          $(element).find('.rating-laboratorio[data-id=' + _this.id + '] input[name=\'rating_rateandgrade[]\']').val('')
        })
      }
  
      //INPUTTEXT
      if (_element.hasClass('_rg_format_text') || _element.hasClass('_rg_format_date')) {
        _element.find('input[type=text]').each(function (index, element) {
          $(element).val('')
          $(element).find('.rating-laboratorio[data-id=' + _this.id + '] input[name=\'rating_rateandgrade\']').val('')
        })
      }
  
      //INPUTIMAGE
      if (_element.hasClass('_rg_format_image')) {
        _element.find('input[type=file]').each(function (index, element) {
          $(element).val('')
          _element.find('.rating-laboratorio[data-id=' + _this.id + '] input[name=\'rating_rateandgrade\']').val('')
        })
      }
  
      //SIGNATURE
      if (_element.hasClass('_rg_format_signature')) {
        _element.find('.rating-laboratorio[data-id=' + _this.id + '] input[name=\'rating_rateandgrade\']').val('')
        if(_element.find('button#clear-signature')){
          _element.find('button#clear-signature').click()
        }
      }
  
    }
  
    this.getElement = function () {
  
      if (_this._element) {
        return _this.element
      }
  
      var _question = $('.grupo-pregunta[data-id="' + this.id + '"]') //Question Laboratorio
      if (_question.length) {
        _this.element = _question
        return _this.element
      }
  
      _question     = $('.pregunta-empleado [data-id="' + this.id + '"]').parent() //Question New Age;
      _this.element = _question
      return _this.element
    }
  
    this.hide = function () {
      _this.getElement().hide()
    }
  
    this.hideTitle = function () {
      var title = _this.getElement().find('h3.pregunta')
      if (title) {
        title.hide()
      }
    }
  
    this.show = function () {
      _this.getElement().show()
    }
  
    this.moveTo = function (_surveyTab) {
      _surveyTab.getPanel().append(this.getElement())
    }
  
    this.getId = function () {
      return this.id
    }
  
    this.getStar = function (_index) {
      var star = $('.rating-laboratorio[data-id="' + this.id + '"] .rating span.star:nth-child(' + _index + ')')
      if (!star.length) {
        return $('.rating-newage[data-id="' + this.id + '"] .rating span.star:nth-child(' + _index + ')').parent() //Question New Age
      }
      return star
    }
  
    this.getNumberStars = function () {
      var starSize = $('.rating-laboratorio[data-id="' + this.id + '"] .rating ,empty-stars span.star').size()
  
      if (!starSize.length) {
        return $('.rating-newage[data-id="' + this.id + '"] .rating ,empty-stars span.star').size() //Question New Age
      }
      return starSize
    }
  
    this.translate = function (lang_code) {
  
      _this.setQuestionTitle(translate(_this.getQuestionTitle().trim(), lang_code))
  
      if (_this.getQuestionText()) {
        _this.setQuestionText(translate(_this.getQuestionText().trim(), lang_code))
      }
  
      var caption = _this.getElement().find('.caption span')
      if (caption) {
        caption.each(function (index, caption) {
          $(this).html(translate($(this).html().trim(), lang_code))
        })
      }
  
      var star_img_content = _this.getElement().find('.empty-stars .star:first-child')
      if(star_img_content) {
        star_img_content.each(function (index, img_content) {
          $(this).attr('style', translate($(this).attr('style'), lang_code))
          })
      }
  
      var star_img = _this.getElement().find('.empty-stars .star:first-child img')
      if(star_img) {
        star_img.each(function (index, img) {
          $(this).attr('src', translate($(this).attr('src'), lang_code))
          })
      }
  
      var radiogroupOption = _this.getElement().find('.radiogroup-element label .label-text')
      if (radiogroupOption) {
        radiogroupOption.each(function (index) {
          $(this).html($(this).html().replace(/<[^>]*>?/g, ''));
          $(this).html(translate($(this).html().trim(), lang_code))
        })
      }
  
      var checkboxOption = _this.getElement().find('input[type="checkbox"]')
      if (checkboxOption) {
        checkboxOption.each(function (index) {
          var label = $(this).checkboxradio('option', 'label')
          if (label.indexOf('</span>') !== -1) {
            label = $(label).html()
          }
          $(this).checkboxradio('option', 'label', translate(label, lang_code))
        })
      }
  
      var texto = _this.getElement().find('#textoSlider')
      if (texto) {
        texto.each(function (index) {
          $(this).html(translate($(this).html().trim(), lang_code))
        })
      }
  
    }
  
    this.getResponseFormat = function () {
      return _this._responseFormat
    }
  
    this.setResponseFormat = function (_format, _options, _format_options, _slider) {
      var _class = _question_response_formats[_format]['class']
      $(this.getElement()).addClass(_class)
  
      if(_options && Object.keys(_options).length) {
        $(this.getElement()).addClass('options-' + Object.keys(_options).length)
      }
  
      if(_format_options && _format_options.class) {
        $(this.getElement()).addClass(_format_options.class)
      }
  
      var response_format = null
      switch (_format) {
        case 'si_no':
          response_format = new ResponseFormatYesOrNo(this, _format, _options)
          response_format.init()
          break
  
        case 'caritas':
          response_format = new ResponseFormatFaces(this, _format, _options)
          response_format.init()
          break
  
        case '6':
          response_format = new ResponseFormat6Options(this, _format, _options)
          response_format.init()
          break
  
        case '5':
          response_format = new ResponseFormat5Options(this, _format, _options)
          response_format.init()
          break
  
        case '4':
          response_format = new ResponseFormat4Options(this, _format, _options)
          response_format.init()
          break
  
        case '3':
          response_format = new ResponseFormat3Options(this, _format, _options)
          response_format.init()
          break
  
        case '2':
          response_format = new ResponseFormat2Options(this, _format, _options)
          response_format.init()
          break
  
        case '1':
          response_format = new ResponseFormat1Option(this, _format, _options)
          response_format.init()
          break
  
        case '9 opciones':
          response_format = new ResponseFormat9Options(this, _format, _options)
          response_format.init()
          break
  
        case '10 opciones':
          response_format = new ResponseFormat10Options(this, _format, _options, null, _slider)
          response_format.init()
          break
  
        case '11 opciones':
          response_format = new ResponseFormat11Options(this, _format, _options, null, _slider)
          response_format.init()
          break
  
        case 'radiogroup':
          response_format = new ResponseFormatRadioGroup(this, _format, _options)
          response_format.init()
          break
  
        case 'checkboxgroup':
          response_format = new ResponseFormatCheckboxGroup(this, _format, _options, _format_options)
          response_format.init()
          break
  
        case 'checkboxgroupthisandthat':
          response_format = new ResponseFormatCheckboxGroupthisandthat(this, _format, _options, _format_options)
          response_format.init()
          break
  
        case 'radiogroupthisandthat':
          response_format = new ResponseFormatRadioGroupthisandthat(this, _format, _options)
          response_format.init()
          break
  
        case 'select':
          response_format = new ResponseFormatSelect(this, _format, _options, _format_options)
          response_format.init()
          break
  
        case 'date':
          response_format = new ResponseFormatInputDate(this, _format, _options, _format_options)
          response_format.init()
          break
  
        case 'textarea':
          response_format = new ResponseFormatTextarea(this, _format, _options, _format_options)
          response_format.init()
          break
  
        case 'inputText':
          response_format = new ResponseFormatInputText(this, _format, _options)
          response_format.init()
          break
  
        case 'signature':
          response_format = new ResponseFormatSignature(this, _format, _options)
          response_format.init()
          break
  
        case 'fileImage':
          response_format = new ResponseFormatFileImages(this, _format, _options, _format_options)
          response_format.init()
          break
  
        case 'numeric':
          response_format = new ResponseFormatNumericInput(this, _options)
          response_format.init()
          break
  
        case 'rtl':
          response_format = new ResponseFormat5OptionsRTL(this, _format, _options)
          response_format.init()
          break
  
        case 'options':
          response_format = new ResponseFormatOptions(this, _format, _options, _format_options)
          response_format.init()
          break
  
        default:
          _my_log('Question.setResponseFormat Formato Desconocido')
      }
      this._responseFormat = response_format
    }
  
    this.getRespoonseValue = function () {
  
      if (_this.getResponseFormat() == null) {
        return this.getElement().find('input[name=\'rating_rateandgrade\']').val()
      }
      return _this.getResponseFormat().getRespoonseValue()
    }
  
    this.setRespoonseValue = function (_value) {
      _this.getResponseFormat().setRespoonseValue(_value)
    }
  
    this.enable = function () {
      _this.getResponseFormat().enable()
    }
  
    this.disable = function (_value) {
      _this.getResponseFormat().disable()
    }
  
    this.setDefaultStarImageOn = function (_img_src) {
      $('#main').ready(function () {
        //FILLED-STARS
        $('.rating-laboratorio[data-id="' + _this.id + '"] .rating .filled-stars span.star img').each(
          function (index) {
            $(this).attr({ style: 'content:url(' + _img_src + ')' })
          }
        )
      })
    }
  
    this.setDefaultStarImageOff = function (_img_src) {
      $('#main').ready(function () {
        //EMPTY-STARS
        $('.rating-laboratorio[data-id="' + _this.id + '"] .rating .empty-stars span.star img').each(
          function (index) {
            $(this).attr({ style: 'content:url(' + _img_src + ')' })
          }
        )
      })
    }
  
    this.setQuestionTitle = function (_question_title) {
      this.getElement().find('h3.pregunta').html(_question_title)
    }
  
    this.getQuestionTitle = function () {
      return this.getElement().find('h3.pregunta').text()
    }
  
    this.setQuestionText = function (_question_text) {
      if (_this.getQuestionText()) {
        _this.getElement().find('p.question-text').text(_question_text)
      } else {
        _this.getElement().find('h3.pregunta').after('<p class="question-text">' + _question_text + '</p>')
      }
    }
  
    this.getQuestionText = function () {
      _question_text = $('.grupo-pregunta[data-id="' + this.id + '"] p.question-text').first()
      if (_question_text) {
        return _question_text.text()
      }
      return null
    }
  
    this.enableLessMoreButton = function () {
      //TODO
    }
  
    this.setSlug = function (slug) {
      this._slug_key = slug
    }
  
    this.getSlug = function () {
      return this._slug_key
    }
  }
  
  function Survey (_modalidad, _campana) {
  
    var _this            = this
    this.modalidad       = _modalidad
    this.campana         = _campana
    this._tabs_modalidad = _tabs_x_modalidad[_modalidad]
    this.actualTab       = 0
  
    //PROGRES_BAR
    this.enabled_progress_bar  = false
    this.selector_progress_bar = null
    this.progressbar           = null
  
    //PAGINATIOM
    this.enabled_pager  = false
    this.selector_pager = null
    this.maxPage_pager  = null
    this.pager          = null
  
    //PRELOAD
    this.IMAGES_TO_PRELOAD_OF_CAMPAIGN = []
    this.ALL_IMAGES_TO_PRELOAD         = []
    this._images                       = []
    this.preload_images_count          = 0
    this._modalSpinner_preload         = $('#modalSpinner').clone().prop('id', 'modalSpinner_preload')
  
    //SURVEY NAVIGATION
    this._enabled_survey_navigation = false
  
    //SURVEY RESPONSE LOAD
    this._enabled_survey_response_load = false
    this._surveyResponse               = null
  
    this.offline = null
  
    this.getSurveyResponse = function () {
      return this._surveyResponse
    }
  
    this.setOffline = function (_offline) {
      this.offline = _offline
    }
  
    this.loadOffline = function () {
      // const offline = this.offline
      // return new Promise(async function (resolve, reject) {
      //   $.holdReady(true)
      //   await offline.registerCache()
      //   $.holdReady(false)
      //
      //   resolve('Success')
      // })
  
    }
  
    this.remove = function (_selector) {
      $(_selector).remove()
    }
  
    this.setImages = function (_img_path_campaign) {
      this.IMAGES_TO_PRELOAD_OF_CAMPAIGN = _img_path_campaign
    }
  
    this.images_campaign = function () {
      return this.IMAGES_TO_PRELOAD_OF_CAMPAIGN
    }
  
    this.allImages = function () {
  
      if (typeof _this.images_campaign() !== 'undefined' && _this.images_campaign() !== null) {
        this.ALL_IMAGES_TO_PRELOAD = _this.getDefaultImages().concat(_this.images_campaign())
        return this.ALL_IMAGES_TO_PRELOAD
      }
  
      this.ALL_IMAGES_TO_PRELOAD = _this.getDefaultImages()
      return this.ALL_IMAGES_TO_PRELOAD
  
    }
  
    this.preloadImages = function (_show_spinner) {
  
      if (typeof _show_spinner === 'undefined' || _show_spinner === null) {
        _show_spinner = true
      }
  
      this.ALL_IMAGES_TO_PRELOAD = _this.allImages()
      if (_this.preload_images_count < this.ALL_IMAGES_TO_PRELOAD.length) {
  
        if (_show_spinner) {
          _this._modalSpinner_preload.modal('show')
        }
  
        for (var i = 0; i < this.ALL_IMAGES_TO_PRELOAD.length; i++) {
          _this._images[i]        = new Image()
          _this._images[i].src    = this.ALL_IMAGES_TO_PRELOAD[i]
          _this._images[i].onload = _this.preloadImageComplete(_show_spinner)
        }
  
      }
  
      $(document).trigger('preload.images.complete')
  
    }
  
    this.preloadImageComplete = function (_show_spinner) {
      _this.preload_images_count = _this.preload_images_count + 1
      if (_this.preload_images_count === _this.ALL_IMAGES_TO_PRELOAD.length) {
        _this.preloadComplete(_show_spinner)
      }
    }
  
    this.preloadComplete = function (_show_spinner) {
  
      if (_show_spinner) {
        _this._modalSpinner_preload.modal('hide')
      }
  
      $(document).trigger('preload.images.complete')
    }
  
    this.getDefaultImages = function () {
  
      var imagesDefault =
            [
              '/img/login/logo_rateandgrade.png',
              '/2.0/img/default/estrella-boton.png',
              // '/assets/31836c4c/img/loading-plugin.gif',
              '/img/login/logo_rateandgrade1.png',
              '/img/login/fondo.jpg',
            ]
  
      return imagesDefault
  
    }
  
    this.getBackgroundImage = function () {
      return $('#main').css('background-image').replace('url("', '').replace('")', '')
    }
  
    this.getLogoImage = function () {
      return $('#logo-navbar').attr('src')
    }
  
    this.init = function () {
      // Reiniciamos inputs - Inspirado en limpiarPantallas
      $('select[name=\'edad\']').val('')
      $('select').val('')
      $('input').val('')
      $('textarea').text('')
      $('textarea').html('')
      $('textarea').val('')
      $('input[type="checkbox"]').prop('checked', true)
      $('.grupo-imagen.img-circle.thisandthat').removeClass('seleccionado')
  
      $('.grupo-imagen.img-circle.thisandthat').css({
                                                      'box-shadow':         '0 2px 9px 1px rgba(0, 0, 0, 0.28)',
                                                      '-webkit-box-shadow': '0 2px 9px 1px rgba(0, 0, 0, 0.28)',
                                                      'border':             '1px solid #ddd !important'
                                                    })
      //$('input[type="radio"]').attr('checked', false).checkboxradio('refresh');
  
    }
  
    this.initTabs = function () {
      var surveyTabs = []
      for (var i in this._tabs_modalidad) {
        switch (this._tabs_modalidad[i]) {
          case 'newage_tab1':
            _surveyTab = new NewageSurveyTab1()
            break
          case 'newage_tab2':
            _surveyTab = new NewageSurveyTab2()
            break
          case 'newage_tab3':
            _surveyTab = new NewageSurveyTab3()
            break
          case 'laboratorio_tab1':
            _surveyTab = new LaboratorioSurveyTab1()
            break
          case 'laboratorio_tab2':
            _surveyTab = new LaboratorioSurveyTab2()
            break
          case 'thisandthat_tab1':
            _surveyTab = new ThisandthatSurveyTab1()
            break
          case 'thisandthat_tab2':
            _surveyTab = new ThisandthatSurveyTab2()
            break
          default:
            _surveyTab = new SurveyTab()
            break
        }
  
        //_surveyTab.setBackgroundImage(_this.getBackgroundImage());
        //_surveyTab.setLogoImage(_this.getLogoImage());
        _surveyTab.original_id = i
        _surveyTab.name        = this._tabs_modalidad[i]
        _surveyTab.setElement($('#' + i + '.tab-pane'))
        surveyTabs[i] = _surveyTab
      }
      return surveyTabs
    }
  
    this.reset = function () {
      _survey.clean()
      _survey.start()
    }
  
    this.clean = function () {
      _this._surveyTabs.forEach(function (tabs) {
        tabs.clean()
      })
    }
  
    this._surveyTabs = this.initTabs()
  
    this.setRgPositiveLogo = function () {
      $('#logo-pie-pagina').attr('src', '/img/logo_rg/RATE-LOGO-POSITIVO.png')
    }
  
    this.setActualTab = function (_actual_tab) {
      _this.actualTab = _actual_tab
    }
  
    this.getLastSurveyTab = function () {
      return this._surveyTabs[this._surveyTabs.length - 1]
    }
  
    this.start = function (lang_code, first_tab) {
      var _name_last_tab = +_this._surveyTabs[_this._surveyTabs.length - 1].deactivate()
      // _this.init();
  
      if (typeof (lang_code) !== 'undefined') {
        _this.translate(lang_code)
      }
  
      _first_tab = 0
      if (first_tab) {
        _first_tab = first_tab
      }
      _this.setActualTab(_first_tab)
  
      $('#main').ready(function () {
        _this.render() //render tabs
  
        for (i = 0; i < _first_tab; i++) {
          _this.getSurveyTabById(i).deactivate()
        }
  
        if (_this.enabled_pager) {
          if (!_this.pager) {
            if (_this.maxPage_pager === null) {
              _this.maxPage_pager = _this._surveyTabs.length
            }
            _this.pager = new Pager(_this.maxPage_pager, _this.selector_pager)
            _this.pager.render()
          }
        }
  
        if (_this.enabled_progress_bar) {
          if (!_this.progressbar) {
            _this.progressbar = new ProgressBar()
            _this.progressbar.render()
          }
          _this.updateProgressBar(null, _this.selector_progress_bar)
        }
  
        if (_this._enabled_survey_navigation) {
          _this._surveyTabs.forEach(function (tab) {
            $('<div class="survey-navigation-bar"></div>').appendTo(tab.getPanel())
            tab.addButton('', 'button', 'btn survey-navigation-btn btn-back', '', 'Atrás', null, true, '<i class="fa fa-arrow-left" aria-hidden="true"></i>', '.survey-navigation-bar')
            tab.setAction('.survey-navigation-btn.btn-back', isTouchstartOrClick(), function () {
              if (saveResponse()) {
                _survey.prevTab()
              }
            }, true)
            tab.getPanel().find('.survey-navigation-bar').append(tab.getPanel().find('.boton-enviar-opinion'))
          })
          _this.getSurveyTabById(_this.actualTab).getPanel().find('.survey-navigation-bar .survey-navigation-btn.btn-back').remove()
          _this._surveyTabs[_this._surveyTabs.length - 1].getPanel().find('.survey-navigation-bar .survey-navigation-btn.btn-back').remove()
        }
  
        $(document).trigger('survey.ready', _this.getSurveyTabById(_this.actualTab))
  
        if (_this._enabled_survey_response_load) {
          var responseRepository = new RestApiSurveyResponseRepository(new RestApiSurveyResponseDatasource())
          _this._surveyResponse  = responseRepository.getResponse(userid, campaignid)
  
          if (_this._surveyResponse && _this._surveyResponse.surveys_response_id) {
            var responseLineRepository = new RestApiSurveyResponseLineRepository(new RestApiSurveyResponseDatasource())
            var surveyResponseLines    = responseLineRepository.getResponseLines(_this._surveyResponse.surveys_response_id)
            _this._filledSurvey(surveyResponseLines)
            if (_this._surveyResponse._survey_response_state_id < 3) {
              _this.goToTab(_lastTab)
            }
          }
        }
  
        if (!_this.getSurveyTabById(_this.actualTab).isVisible()) {
          _this.nextTab()
        } else {
          _this.getSurveyTabById(_this.actualTab).activate()
        }
  
        $(document).trigger('surveystart', _this.getSurveyTabById(_this.actualTab)) //Deprecated
        $(document).trigger('survey.start', _this.getSurveyTabById(_this.actualTab))
      })
    }
  
    this._filledSurvey = function (jsonData) {
      jsonData = JSON.parse(jsonData)
  
      var questionId = null
  
      jsonData.forEach(function (element) {
  
        var value
        if (element['question_value_type'] === 'integer') {
          if (Array.isArray(element['value_number'])) {
            value = element['value_number']
          } else {
            value = parseInt(element['value_number'])
          }
        } else {
          value = element['value_varchar']
        }
  
        // if (!questionId || element['question_id'] != questionId ) {
        // var filterTab = _this._surveyTabs.filter(tab => tab._tabQuestions && tab._tabQuestions[element['question_id']])
        // filterTab[0]._tabQuestions[element['question_id']].setRespoonseValue(value)
        // questionId = element['question_id']
        var filterTab = _this._surveyTabs.filter(
          function (tab) {
            return tab._tabQuestions && tab._tabQuestions[element['question_id']]
          })
        filterTab[0]._tabQuestions[element['question_id']].setRespoonseValue(value)
        questionId = element['question_id']
        // }
  
      })
    }
  
    this.getTabs = function () {
      return this._tabs_modalidad
    }
  
    this.setFormatResponseToQuestions = function (idQuestions, formato, options, _slider) {
      var _question_
      idQuestions.forEach(function (element) {
        _question_ = new Question(element)
        _question_.setResponseFormat(formato, options, null, _slider)
      })
    }
  
    this.getSurveyTabs = function () {
      return this._tabs_modalidad
    }
  
    this.getSurveyTabById = function (_position) {
      return this._surveyTabs[_position]
    }
  
    this.getSurveyTabByName = function (_name) {
      var position = this.getSurveyTabPosition(_name)
      return this._surveyTabs[position]
    }
  
    this.translate = function (lang_code) {
      //_my_log('TRANSLATE | Survey.translate | lang_code:'+lang_code);
      this._surveyTabs.forEach(function (element, index) {
        _this.getSurveyTabById(index).translate(lang_code)
      })
    }
  
    this.hideAllTabs = function () {
      $('div.tab-pane').removeClass('in')
      $('div.tab-pane').removeClass('active')
    }
  
    this.addNewSurveyTab = function (name, type) {
  
      var number_tabs = this._surveyTabs.length
  
      tab_html = '<div id="' + number_tabs + '" class="tab-pane fade">' +
        '<div class="row flex-panels">' +
        '<div class="col-lg-9 col-md-9 col-sm-12 col-xs-12 centrado">' +
        '<div class="panel panel-central" style="' + _panel_styles + '">' +
        '</div>' +
        '</div>' +
        '</div>' +
        '</div>'
      $(tab_html).insertAfter('#' + (number_tabs - 1))
  
      var new_surveytab = new SurveyTab()
  
      if (type) {
        switch (type) {
          case 'newage_tab1':
            new_surveytab = new NewageSurveyTab1()
            break
          case 'newage_tab2':
            new_surveytab = new NewageSurveyTab2()
            break
          case 'laboratorio_tab1':
            new_surveytab = new LaboratorioSurveyTab1()
            break
          case 'laboratorio_tab2':
            new_surveytab = new LaboratorioSurveyTab2()
            break
          case 'thisandthat_tab1':
            new_surveytab = new ThisandthatSurveyTab1()
            break
          case 'thisandthat_tab2':
            new_surveytab = new ThisandthatSurveyTab2()
            break
          default:
            new_surveytab = new SurveyTab()
            break
        }
      } else {
        new_surveytab = new SurveyTab()
      }
  
      new_surveytab.original_id = -1 //Originalmente este tab no existe
      new_surveytab.name        = name
      new_surveytab.setElement($('#' + number_tabs + '.tab-pane'))
      this._surveyTabs.push(new_surveytab)
  
      var _new_tab_position = number_tabs
      return _new_tab_position
    }
  
    this.addNewSurveyTabInPosition = function (name, position, type) {
      var _new_tab_position = this.addNewSurveyTab(name, type)
      this.moveSurveyTabById(_new_tab_position, position)
      return this._surveyTabs[position]
    }
  
    this.addSurveyTab = function (_tab, position) {
      this._surveyTabs.push(_tab)
      this.moveSurveyTabById(this._surveyTabs.length - 1, position)
    }
  
    this.moveSurveyTabById = function (_position, _position_new) {
  
      _tab = this._surveyTabs[_position]
  
      //Reindexamos los ids de las tabs del DOM
      for (i = this._surveyTabs.length - 1; i >= _position_new; i--) {
        var _pos = i + 1
        this._surveyTabs[i].getElement().attr('id', _pos)
      }
      _tab.getElement().attr('id', _position_new)
  
      var _pos = _position_new + 1
      if (_position >= _position_new) {
        _tab.getElement().insertBefore($('#' + _pos + '.tab-pane'))
      } else {
        _tab.getElement().insertAfter($('#' + _pos + '.tab-pane'))
      }
  
      if (_position >= this._surveyTabs.length) {
        var k = _position - this._surveyTabs.length
        while ((k--) + 1) {
          this._surveyTabs.push(undefined)
        }
      }
      this._surveyTabs.splice(_position_new, 0, this._surveyTabs.splice(_position, 1)[0])
  
      // this.render(); //Para actualizar la salida
  
    }
  
    this.moveSurveyTabByName = function (_name, _position_new) {
      var position = this.getSurveyTabPosition(_name)
      this.moveSurveyTabById(position, _position_new)
    }
  
    this.deleteSurveyTabById = function (_position) {
  
      //Update en código
      _tab = this._surveyTabs[_position]
      _tab.getElement().remove()
  
      //Update Array SurveyTabs
      this._surveyTabs.splice(_position, 1)
  
      this.render()
    }
  
    this.deleteSurveyTabByName = function (_name) {
      var position = this.getSurveyTabPosition(_name)
      this.deleteSurveyTabById(position)
    }
  
    this.getSurveyTabPosition = function (_name, _position_new) {
  
      var position = -1
      this._surveyTabs.forEach(
        function (surveyTab, index) {
          if (surveyTab.name == _name) {
            position = index
          }
        }
      )
      return position
    }
  
    this.nextTab = function () {
      var _active_tab = false
  
      while (_active_tab === false) {
        var _actual = _this.actualTab
        _this.getSurveyTabById(_actual).deactivate()
        var _siguiente  = parseInt(_this.actualTab) + 1
        _active_tab     = _this.getSurveyTabById(_siguiente).activate()
        _this.actualTab = _siguiente
  
        if (_this.enabled_pager) {
          _this.updatePager(null, _this.selector_pager)
        }
  
        if (_this.enabled_progress_bar) {
          _this.updateProgressBar(null, _this.selector_progress_bar)
        }
      }
      $(document).trigger('survey.nextTab', _this.getSurveyTabById(_siguiente))
    }
  
    this.prevTab = function () {
      var _active_tab = false
      while (_active_tab === false && _this.actualTab > 0) {
        var _actual = _this.actualTab
        _this.getSurveyTabById(_actual).deactivate()
        var _anterior   = parseInt(_this.actualTab) - 1
        _active_tab     = _this.getSurveyTabById(_anterior).activate()
        _this.actualTab = _anterior
  
        if (_this.enabled_pager) {
          _this.updatePager(null, _this.selector_pager)
        }
  
        if (_this.enabled_progress_bar) {
          _this.updateProgressBar(null, _this.selector_progress_bar)
        }
  
      }
      $(document).trigger('survey.prevTab', _this.getSurveyTabById(_anterior))
    }
  
    this.firstTab = function () {
      var _active_tab = false
      var _firstTab   = 0
      while (_active_tab === false && _this.actualTab > 0) {
        var _actual = _this.actualTab
        _this.getSurveyTabById(_actual).deactivate()
        _active_tab = _this.getSurveyTabById(_firstTab).activate()
        _my_log('>>>actual:' + _firstTab + '>>>anterior:' + _actual + '>>>active-tab:' + _active_tab)
        _this.actualTab = _firstTab
  
        if (_this.enabled_pager) {
          _this.updatePager(null, _this.selector_pager)
        }
  
        if (_this.enabled_progress_bar) {
          _this.updateProgressBar(null, _this.selector_progress_bar)
        }
      }
    }
  
    this.goToTab = function (_tab) {
      var _active_tab = false
      while (_active_tab === false && _this.actualTab >= 0) {
        var _actual = _this.actualTab
        _this.getSurveyTabById(_actual).deactivate()
        _active_tab = _this.getSurveyTabById(_lastTab).activate()
        _my_log('>>>actual:' + _tab + '>>>anterior:' + _actual + '>>>active-tab:' + _active_tab)
        _this.actualTab = _tab
  
        if (_this.enabled_pager) {
          _this.updatePager(null, _this.selector_pager)
        }
  
        if (_this.enabled_progress_bar) {
          _this.updateProgressBar(null, _this.selector_progress_bar)
        }
      }
    }
  
    this.render = function () {
      _this._surveyTabs.forEach(
        function (surveyTab, index) {
          surveyTab.render(index)
  
          //TODO 16.01.2019 BORRAR (ya se hace en start)
          if (index == 0) {
            if (!$(surveyTab.getElement()).hasClass('active')) {
              $(surveyTab.getElement()).addClass('in')
              $(surveyTab.getElement()).addClass('active')
            }
          } else {
            if ($(surveyTab.getElement()).hasClass('active')) {
              $(surveyTab.getElement()).removeClass('in')
              $(surveyTab.getElement()).removeClass('active')
            }
          }
        }
      )
  
    }
  
    this.test = function () {
      return 'Survey echo test'
    }
  
    this.addHeaderButton = function (_id, _text, _class, _style, _icon_fa, _alt) {
      var _button = '<button type="button" id="' + _id + '" name="' + _id + '" title="' + _alt + '" class="boton-navegacion ' + _class + ' style="' + _style + '">' +
        '<i class="fa ' + _icon_fa + '" aria-hidden="true"></i><span>' + _text + '</span>'
      '</button>'
      return $(_button).appendTo($('#myNavbar .container-fluid'))
    }
  
    this.addLogoutButton = function (_id, _title, _subtitle, _text) {
      var _buttonElement = this.addHeaderButton(
        "logoutbutton",
        "",
        "logoutButton",
        "",
        "fa-sign-out-alt"
      );
      // $('#logoutbutton').on('click', function() {
      _buttonElement.on('click', function () {
        $('#boton-envio-login').hide()
        $('#boton-envio-logout').show()
        $('#modalLogin').modal('show')
      })
      return _buttonElement
    }
  
    this.addLogoutNoUserButton = function () {
      var _buttonElement = this.addHeaderButton(
        "logoutNoUserButton",
        "",
        "logoutButton",
        "",
        "fa-sign-out-alt"
      );
      // $('#logoutNoUserButton').on('click', function() {
      _buttonElement.on('click', function () {
        window.location.href = '/site/logoutnouser'
      })
      return _buttonElement
    }
  
    this.addReloadButton = function () {
      var _buttonElement = this.addHeaderButton(
        "reloadbutton",
        "",
        "reloadButton",
        "",
        "fa-sync"
      );
      var _behaviours    = new Behaviour(_this)
      // $('.reloadButton').on('click',_behaviours.reloadPage);
      _buttonElement.on('click', _behaviours.reloadPage)
      return _buttonElement
    }
  
    this.addQuestionButton = function () {
      this.addHeaderButton('questionbutton', '', 'questionbutton', '', 'fa-question-circle')
    }
  
    this.addCreatemeetingButton = function () {
      this.addHeaderButton('meetingButton', 'Reunión', 'meetingButton', '', 'fa-plus-circle')
    }
  
    this.addChangeCampaignButton = function () {
      this.addHeaderButton('changecampaignbutton', '', 'changeCampaignButton', '', 'fa-exchange')
  
    }
  
    this.addShareWifiButton = function () {
      if (typeof Android !== 'undefined') {
        this.addHeaderButton('sharewifibutton', '', 'shareWifiButton', '', 'fa-wifi')
  
        $('.shareWifiButton').on('click',
                                 function () {
                                   if ($(this).hasClass('on')) {
                                     Android.setWiFiHotspotEnabled(false)
                                     $(this).removeClass('on')
                                   } else {
                                     Android.setWiFiHotspotEnabled(true)
                                     $(this).addClass('on')
                                   }
                                 }
        )
      }
    }
  
    this.changeMenuButton = function () {
      var _menubutton = $('button.boton-navegacion').first()
      _menubutton.find('span').remove()
      _menubutton.append('<i class="fa fa-bars" aria-hidden="true"></i>')
    }
  
    this.addPlayButton = function (_modal_name) {
  
      var _playButton = '<button type="button" class="boton-navegacion playButton" id="playbutton"><i class="fa fa-play" aria-hidden="true"></i></button>'
      $('#myNavbar .container-fluid').append(_playButton)
  
      if (_modal_name) {
        $('.playButton').on('click', function () {
                              $('#' + _modal_name).modal('show')
                            }
        )
      } else {
        if (typeof Android === 'undefined') {
          _my_log('Android > undefined')
        } else {
          _my_log('Android > OK')
          $('.playButton').on('click', function () {
                                Android.startScreensaver()
                              }
          )
        }
      }
    }
  
    this.enablePager = function (maxPage, selector) {
  
      if (typeof maxPage === 'undefined') {
        maxPage = null
      }
      if (typeof selector === 'undefined') {
        selector = null
      }
  
      _this.enabled_pager  = true
      _this.maxPage_pager  = maxPage
      _this.selector_pager = selector
    }
  
    this.disablePager = function () {
      this.enabled_pager = false
    }
  
    this.updatePager = function (value, selector) {
  
      if (typeof value === 'undefined') {
        value = null
      }
      if (typeof selector === 'undefined') {
        selector = null
      }
  
      if (typeof selector !== 'undefined' && selector !== null) {
        selector.append(this.pager.getElement())
      } else {
        if (typeof _this.selector_pager !== 'undefined' && _this.selector_pager !== null) {
          _this.selector_pager.append(this.pager.getElement())
        }
      }
  
      if (this.pager) {
  
        if (typeof value === 'undefined' || value === null) {
          value = parseInt(_this.actualTab) + 1
        }
  
        if (value > this.pager._maxPage) {
          this.pager.updateValue(this.pager._maxPage)
        } else {
          this.pager.updateValue(value)
        }
  
      }
  
    }
  
    this.enableProgressBar = function (selector) {
  
      if (typeof selector === 'undefined') {
        selector = null
      }
  
      this.enabled_progress_bar  = true
      this.selector_progress_bar = selector
    }
  
    this.disableProgressBar = function () {
      this.enabled_progress_bar = false
    }
  
    this.enableSurveyNavigation = function () {
      this._enabled_survey_navigation = true
    }
  
    this.disableSurveyNavigation = function () {
      this._enabled_survey_navigation = false
    }
  
    this.enableSurveyResponseLoad = function () {
      this._enabled_survey_response_load = true
    }
  
    this.disableSurveyResponseLoad = function () {
      this._enabled_survey_response_load = false
    }
  
    this.updateProgressBar = function (percent, selector) {
  
      if (typeof value === 'undefined') {
        value = null
      }
      if (typeof selector === 'undefined') {
        selector = null
      }
  
      //Mover a la Tab Actual
      //_my_log('UPDATEPROGRESSBAR | _this.actualTab:'+_this.actualTab);
      var _panel_actual_tab = _this._surveyTabs[_this.actualTab].getPanel()
  
      if (typeof selector !== 'undefined' && selector !== null) {
        selector.append(this.progressbar.getElement())
      } else {
        if (typeof _this.selector_progress_bar !== 'undefined' && _this.selector_progress_bar !== null) {
          _this.selector_progress_bar.append(this.progressbar.getElement())
        } else {
          if (_panel_actual_tab.find('button').length > 0) {
            this.progressbar.getElement().insertBefore(_panel_actual_tab.find('button').first())
          } else {
            _panel_actual_tab.append(this.progressbar.getElement())
          }
        }
      }
  
      if (this.progressbar) {
        if (typeof percent !== 'undefined' && percent !== null) {
          this.progressbar.updateValue(percent)
        } else {
          if (_this.actualTab === _this._surveyTabs.length - 1) {
            this.progressbar.updateValue(100)
          } else {
            this.progressbar.updateValue(parseInt((_this.actualTab / _this._surveyTabs.length) * 100))
          }
        }
      }
  
    }
  
    this.save = function () {
      $(document).trigger('surveybeforesave')
      callActionSaveData()
    }
  
    this.setBackgroundImage = function (_url) {
      _my_log('| Survey.setBackgroundImage: ' + _url)
      this.background_image = _url
      $(document).ready(function () {
        $('div#main').css('background-image', 'url(\'' + _url + '\')')
      })
    }
  
    this.reload = function () {
      if (navigator.onLine) {  //Recargar Página / Timeout
        if (_this !== null && _this.offline !== null) {
          if (_this.offline.isOffline) {
            _this.offline.pushQueue()
            _this.offline.unregisterSw()
          }
        }
        window.location.reload(true)
      } else {
        _this.reset()
      }
    }
  }
  
  function Button (_id, _type, _class, _style, _text, _image, _content) {
  
    this.id        = _id
    this.type      = _type
    this.class     = _class
    this.style     = _style
    this.text      = _text
    this.surveytab = null
  
    if (typeof _image === 'undefined' || _image == null) {
      this.image = 'img/estrella-boton.png'
    } else {
      this.image = _image
    }
  
    if (typeof _content !== 'undefined') {
      this.content = _content
    }
  
    this.setSurveyTab = function (_surveytab) {
      this.surveytab = _surveytab
    }
  
    this.getSurveyTab = function () {
      return this.surveytab
    }
  
    this.render = function () {
  
      var _content_html = null
  
      if (typeof this.content !== 'undefined') {
        _content_html = this.content
      } else {
        _content_html = '<img id="icon-btn-opinion" src="' + this.image + '">' + _text
      }
  
      return '<button ' +
        '   type="' + this.type + '"' +
        '   id="' + this.id + '"' +
        '   name="' + this.id + '"' +
        '   class="' + this.class + '"' +
        '   style="' + this.style + '"' +
        '>' +
        _content_html +
        '</button>'
  
    }
  
    this.activate = function () {
      //TO-DO
    }
  }
  
  function Modal (_id, _title, _subtitle, _text, _show_header) {
  
    var _container
    var _modal      = ''
    var show_header = true
  
    if (!_show_header) {
      show_header = false
    }
  
    if (show_header) {
      _modal = '<div class="modal fade" id="' + _id + '" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">' +
        '	<div class="modal-dialog" role="document">' +
        '        <div class="modal-content">' +
        '    	    <div class="modal-header modal-terminos">' +
        '        		<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true"><i class="fa fa-times"></i></span> <span class="close-txt">Cerrar</span></button>' +
        '    			<div class="modal-titulos-terminos">' +
        '        			<h4 class="modal-title modal-terminos" id="myModalLabel"><b>' + _title + '</b></h4>' +
        '    				<p class="modal-subtitle modal-terminos">' + _subtitle + '</p>' +
        '    			</div>' +
        '    		</div>' +
        '    		<div class="modal-body modal-terminos">' + _text + '</div>' +
        '    	</div>' +
        '    </div>' +
        '</div>'
    } else {
      _modal = '<div class="modal fade" id="' + _id + '" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">' +
        '	<div class="modal-dialog" role="document">' +
        '        <div class="modal-content">' +
        '    	    <div class="modal-header modal-terminos">' +
        '        		<span type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true"><i class="fa fa-times"></i></span> <span class="close-txt">Cerrar</span></button>' +
        '    			</div>' +
        '    		<div class="modal-body modal-terminos">' + _text + '</div>' +
        '    	</div>' +
        '    </div>' +
        '</div>'
    }
  
    _container = $('.wrap .container')
    _container.append(_modal)
    return _container.find('#' + _id)
  }
  
  function ProgressBar () {
  
    var _this = this
  
    this._element = null
  
    this.updateValue = function (_progress_value) {
      var _progressbar = $('#progressbar')
  
      _progressbar.progressbar({
                                 value: _progress_value
                               })
      _progressbar.find('.progress-label').html(_progress_value + '%')
    }
  
    this.render = function () {
      var _header_progress_bar = '' +
        '<div id="progressbar"><div class="progress-label"></div></div>'
      _this._element           = $(_header_progress_bar).insertBefore('.tab-content')
      _this.updateValue(0)
    }
  
    this.getElement = function () {
      return this._element
    }
  }
  
  function Pager (_maxPage, selector) {
  
    if (typeof selector === 'undefined') {
      selector = null
    }
  
    var _this = this
  
    this._maxPage  = _maxPage
    this._selector = $('#myNavbar')
    this._element  = null
  
    if (typeof selector !== 'undefined' && selector !== null) {
      this._selector = selector
    }
  
    this.updateValue = function (_value) {
      var _pager = $('#pager')
      _pager.find('.actualPage').html(_value)
    }
  
    this.render = function () {
      var _body_pager = '' +
        '<div id="pager">Página <span class="actualPage">1</span> de <span class="maxPage">' + _this._maxPage + '</span></div>'
      _this._element  = _this._selector.append(_body_pager)
  
    }
  
    this.getElement = function () {
      return this._element
    }
  
  }
  
  function buttonTranslate (button_element, lang_code) {
    _img = $(button_element).find('img')
    if (_img) {
      _img_tmp = _img.clone()
      _img.remove()
    }
    _translated_text = translate($(button_element).html().trim(), lang_code)
    $(button_element).html(_translated_text)
    if (_img) {
      $(button_element).prepend(_img_tmp)
    }
  
  }
  
  function RestApiSurveyResponseDatasource () {
  
    var _this                 = this
    this._rest_api_host       = _host + '/'
    this._rest_api_path       = this._rest_api_host + 'surveys/'
    this._rest_api_token      = 'oauth2/token/'
    this._rest_api_introspect = 'oauth2/introspect/'
  
    this._rest_api_credentials = {}
  
    this._endpoints                   = []
    this._endpoints['responses']      = 'response/'
    this._endpoints['response-lines'] = 'response/response-lines/'
  
    this.getEndpoint = function (name) {
      return this._rest_api_path + this._endpoints[name]
    }
  
  }
  
  function RestApiSurveyResponseRepository (datasource) {
  
    this._datasource = datasource
  
    this.getResponse = function (user_id, campaign_id) {
      var _result = null
  
      $.ajax({
               type:        'GET',
               url:         this._datasource.getEndpoint('responses') + '?user_id=' + userid + '&survey_id=' + campaign_id,
               contentType: 'application/json',
               async:       false,
               success:     function (data) {
                 if (data != 404) {
                   _result = data
                 }
               },
               error:       function (exception) {
                 console.debug(exception)
                 _modalError_modal.modal('show')
                 _result = false
               },
               statusCode:  {
                 201: function (argument) {
                   console.debug('Correcto')
                 },
                 404: function () {
                   console.debug('No hay nada')
                 },
                 401: function () {
                   console.debug('Expiro el Token')
                 },
                 403: function () {
                   console.debug('No tiene permiso')
                 }
               }
             })
  
      return _result
    }
  
  }
  
  function RestApiSurveyResponseLineRepository (datasource) {
  
    this._datasource = datasource
  
    this.getResponseLines = function (surveys_response_id) {
      var _result = true
  
      $.ajax({
               type:        'GET',
               url:         this._datasource.getEndpoint('response-lines') + '?surveys_response_id=' + surveys_response_id,
               contentType: 'application/json',
               async:       false,
               success:     function (data) {
                 console.debug(JSON.parse(data))
                 _result = data
               },
               error:       function (exception) {
                 console.debug(exception)
                 _modalError_modal.modal('show')
                 _result = false
               },
               statusCode:  {
                 201: function (argument) {
                   console.debug('Correcto')
                 },
                 404: function () {
                   console.debug('No hay nada')
                 },
                 401: function () {
                   console.debug('Expiro el Token')
                 },
                 403: function () {
                   console.debug('No tiene permiso')
                 }
               }
             })
  
      return _result
    }
  
  }
  
  